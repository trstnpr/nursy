<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NurseType extends Model {

	/**
     * The database table.
     *
     */
    protected $table = 'nurse_type';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nurse'];

}

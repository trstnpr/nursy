<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    /**
     * The database table.
     *
     */
    protected $table = 'users';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'image', 'first_name', 'middle_name', 'last_name', 'gender', 'civil_status', 'birthdate', 'street', 'barangay', 'town', 'province', 'degree', 'year_graduated', 'school', 'license', 'about', 'background', 'cellphone', 'landline', 'email', 'nurse_type', 'services', 'password', 'email_activation_code', 'active', 'role_id', 'password_recover'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NurseAccountRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
			'birthdate' => 'required',
            'civil_status' => 'required',
            'street' => 'required',
            'barangay' => 'required',
            'town' => 'required',
            'province' => 'required',
			'degree' => 'required',
			'year_graduated' => 'required',
			'school' => 'required',
			'license' => 'required',
            'cellphone' => 'required'
        ];
    }
}
<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\Http\Requests\AccountRequest;
use App\Http\Requests\PasswordRequest;


use Illuminate\Http\Request;

//Models
use App\User;

//Module Controllers
use App\Modules\Nurse\Controllers\NurseController;
use App\Modules\Patient\Controllers\PatientController;
use App\Modules\Schedule\Controllers\ScheduleController;
use App\Modules\Frontend\Controllers\FrontendController;

class HomeController extends Controller
{
    private $nurse;
    private $patient;

    /**
     * Create a new controller instance.
     *
     * @return voidStack Overflow is a community of 4.7 million programmers, just like you, helping each other. 


     */
    public function __construct()
    {
        //$this->middleware('auth');
        $this->frontend = new FrontendController;
        $this->nurse = new NurseController;
        $this->patient = new PatientController;
        $this->schedule = new ScheduleController;
    }

    public function uploadProfileImage(Request $request)
    {
        //DELETE PREVIOUS PROFILE IMAGE TO FREE STORAGE SPACE
        $prev_image = User::where('id', '=', Auth::user()->id)->value('image');
        $file_path = $prev_image;
        if($file_path != null){
            $this->removeFile($file_path);
        }

        //ADD PROFILE IMAGE TO AMAZON WEB SERVICES
        $file = $request->file('profile_image');
        $extension = $file->getClientOriginalExtension();
        $file_name = Auth::user()->id.'/'.str_random(8).'.'.$extension;
        if(\Storage::disk('s3')->put($file_name,  \File::get($file), 'public')){
            //$mime = $file->getClientMimeType();
            $inputImage = array('image' => $file_name);
            $result = User::where('id', '=', Auth::user()->id)->update($inputImage);
            if($result){
                return json_encode(array('result'=>'success'));
            } else {
                return json_encode(array('result'=>'There is an error uploading your Profile Image!'));
            }
        } else {
            return json_encode(array('result'=>'There is an error uploading your Profile Image!'));
        }
    }

    public function removeFile($file_path)
    {
        if (\Storage::disk('s3')->has($file_path)){
            \Storage::disk('s3')->delete($file_path);
        }
        return true;
    }
	
	// DHTMLX
	public function dhtmlx()
	{
		if(Auth::user()->role_id == 2){
            return $this->nurse->dhtmlx();
        }
	}

    //Fetch Nurse Schedule For Zabuto Calendar
    public function getNurseSchedule($sort, $id)
    {
        return $this->schedule->getNurseSchedule($sort, $id);
    }

    //Reserve Nurse Schedule For Patient
    public function reserveNurseSchedule(Request $req)
    {
        if(Auth::user()->role_id == 3){
            return $this->schedule->reserveNurseSchedule($req);
        }
    }

    //Approve or Decline Schedule for Nurse
    public function appointmentAction(Request $req){
        if(Auth::user()->role_id == 2){
            return $this->schedule->appointmentAction($req);
        }
    }
}
<?php namespace App\Modules\Feedback\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model {

	/**
     * The database table.
     *
     */
    protected $table = 'feedbacks';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nurse_id', 'patient_id', 'feedback', 'rating'
    ];

}

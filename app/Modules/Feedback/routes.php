<?php

Route::group(array('module' => 'Feedback', 'namespace' => 'App\Modules\Feedback\Controllers'), function() {

    Route::resource('Feedback', 'FeedbackController');
    
});	
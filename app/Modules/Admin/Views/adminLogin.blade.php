@extends('layouts.app')

@section('content')
<section id="login">
    <div class="wrap">
        <div class="container">
            <div class="row login-page">
              <div class="col m4 s12 offset-m4">
                    <div class="center">
                        <img src="{{ asset('assets/images/logo/nursy-logo-2.png') }}" alt="" class="responsive-img">
                        <p class="center login-form-text">NURSY - ADMIN</p>
                    </div>

                    <form class="" role="form" method="post" id="adminLoginForm">
                      {!! csrf_field() !!}
                      <div class="row card-panel blue-grey darken-4">
                          <div class="input-field col s12">
                              <input type="text" id="username" name="username" required="required" placeholder="Username"/>
                              <label for="username">Username</label>
                          </div>
                          <div class="input-field col s12">
                              <input type="password" id="password" name="password" required="required" placeholder="Your password"/>
                              <label for="password">Password</label>
                          </div>
                          <div class="col s12">
                              <input type="checkbox" class="filled-in" id="remember-me" name="remember" />
                              <label for="remember-me">Remember me</label>
                          </div>
                          <div class="input-field col s12 hide" id="error_div">
                              <div class="card-panel pink ligthen-2">
                                  <strong><i class="mdi-alert-warning"></i> Error Message</strong>
                                  <ul class="error_list">
                                  </ul>
                              </div>
                          </div>
                          <div class="input-field col s12">
                              <button type="submit" id="login_submit" class="btn btn-large waves-effect waves-light blue-grey col s12">Login</button>
                          </div>
                      </div>

                      <div class="row">
                          <div class="input-field">
                              <p class="center"><a href="{{ url('/') }}" class="white-text">Get Back Home</a></p>
                          </div>        
                      </div>

                  </form>
              </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('customScript')
<script type="text/javascript">
  $(document).ready(function() {
    $("#adminLoginForm").on('submit',(function(e) {
      e.preventDefault();
      $.ajax({
        url: "{{url('admin/login')}}", // Url to which the request is send
        type: "POST",             // Type of request to be send, called as method
        data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
        contentType: false,       // The content type used when sending data to the server.
        cache: false,             // To unable request pages to be cached
        processData:false,        // To send DOMDocument or non processed data file it is set to false
        beforeSend: function(){ $("#login_submit").html('Processing...');},
        error: function(data){
          console.log(data);
          if(data.readyState == 4){
              errors = JSON.parse(data.responseText);
              $('.error_list').empty();
              $.each(errors,function(key,value){
                $('.error_list').append('<li>'+value+'</li>');
              });
              $('#error_div').removeClass('hide');
          }
          $("#login_submit").html('Login');
        },
        success: function(data)   // A function to be called if request succeeds
        {
          console.log(data);
          var msg = JSON.parse(data);
          if(msg.result == 'success'){
            $("#login_submit").html('Login');
            window.location.href = "{{url('admin')}}/"+msg.dialog;
          } else{
            $("#password").val('');
            $("#login_submit").html('Login');
            $("#error_div").removeClass('hide');
            $(".error_list").html('<li>'+msg.dialog+'</li>');
          }
        }
      });
    }));
  });
</script>
@stop

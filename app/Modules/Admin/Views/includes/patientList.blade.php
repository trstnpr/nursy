<div class="section">
	<p class="caption">List of Patient</p>
	<div class="divider" id="divhr"></div>
	<div class="row">
		<div class="col s12 m12 l12">
			<div class="card-panel">
				<table class="bordered highlight centered" id="patientList">
					<thead>
						<tr>
							<th>ID no.</th>
							<th data-field="name">Name</th>
							<th>Email</th>
							<th>Date joined</th>
							<th>Edit</th>
							<th>Remove</th>
						</tr>
					</thead>

					<tbody>
						@if($patient->count() == 0)
						<tr>
							<td colspan="5" align="center">NO NURSE FOUND</td>
						</tr>
						@else
							@foreach($patient as $n)
							<tr>
								<td>{{$n->id}}</td>
								<td>{{$n->first_name.' '.$n->last_name}}</td>
								<td>{{$n->email}}</td>
								<td>{{date_format($n->created_at, 'F d Y')}}</td>
								<td>
									<button class="btn-floating btn-flat blue-grey" onClick="updatePatient({{$n->id}});"><i class="mdi-editor-mode-edit"></i></button>
								</td>
								<td>
									<button class="btn-floating btn-flat red" onClick="deletePatient({{$n->id}});"><i class="mdi-navigation-close"></i></button>
								</td>
							</tr>
							@endforeach
						@endif
					</tbody>

				</table>
			</div>
		</div>
	</div>
</div>
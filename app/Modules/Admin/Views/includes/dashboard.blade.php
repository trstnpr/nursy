<div class="section">
	<h3>Dashboard</h3>
	<div class="divider" id="divhr"></div>
	<div id="card-stats">
		<div class="row">
			<div class="col s12 m6 l4">
				<div class="card">
					<div class="card-content green white-text">
						<p class="card-stats-title"><i class="mdi-social-group-add"></i> Patients</p>
						<h4 class="card-stats-number">{{$patient->count()}}</h4>
						<p class="card-stats-compare"><span class="green-text text-lighten-5">Registered</span></p>
					</div>
					<div class="card-action green darken-2"></div>
				</div>
			</div>
			
			<div class="col s12 m6 l4">
				<div class="card">
					<div class="card-content light-blue white-text">
						<p class="card-stats-title"><i class="mdi-social-group-add"></i> Nurse</p>
						<h4 class="card-stats-number">{{$nurse->count()}}</h4>
						<p class="card-stats-compare"><span class="green-text text-lighten-5">Registered</span></p>
					</div>
					<div class="card-action light-blue darken-2"></div>
				</div>
			</div>
			
			<div class="col s12 m6 l4">
				<div class="card">
					<div class="card-content blue-grey white-text">
						<p class="card-stats-title"><i class="mdi-social-group-add"></i> User</p>
						<h4 class="card-stats-number">{{ $nurse->count() + $patient->count()}}</h4>
						<p class="card-stats-compare"><span class="green-text text-lighten-5">Registered</span></p>
					</div>
					<div class="card-action blue-grey darken-2"></div>
				</div>
			</div>
		</div>
	</div>

	

	<div id="work-collections">
		<div class="row">
			<div class="col s12 m12 l12">
				<h4 class="header">Booking List</h4>
				<ul id="projects-collection" class="collection">

					<li class="collection-item avatar">
						<i class="mdi-action-schedule circle blue-grey"></i>
						<span class="collection-header">Latest Booking</span>
						<p>100</p>
					</li>

					<li class="collection-item" style="padding: 0px 0px 0px 0px !important;">

						<div style="overflow-y:auto;">
							
							<!-- NO DOCTOR YET REGISTERED -->
							<div class="hide" align="center" style="padding:100px 0px 0px 0px;">
								<h4 class="thin grey-text">NO BOOKING FOUND</h4>
							</div>
						   
							<!-- LIST DOCTOR -->
						   
							<table class="bordered highlight responsive-table centered">
								<thead>
									<tr>
										<th data-field="id">Book ID</th>
										<th data-field="id">Patient Name</th>
										<th data-field="name">Booked Nurse Name</th>
										<th data-field="price">Arrival Date & Time</th>
										<th data-field="price">Recent time</th>
									</tr>
								</thead>

								<tbody>
									<!-- looping -->
									<tr>
										<td>1</td>
										<td>Juana Dela Cruz</td>
										<td>Jd Sarmiento</td>
										<td>
											<p class="collections-content">Jan 01, 2016 - 9:00 AM</p>
										</td>
										<td>
											<p class="collections-content">2 hours ago</p>
										</td>
									</tr>

									<tr>
										<td>2</td>
										<td>Juana Dela Cruz</td>
										<td>Jd Sarmiento</td>
										<td>
											<p class="collections-content">Jan 01, 2016 - 9:00 AM</p>
										</td>
										<td>
											<p class="collections-content">2 hours ago</p>
										</td>
									</tr>

									<tr>
										<td>3</td>
										<td>Juana Dela Cruz</td>
										<td>Jd Sarmiento</td>
										<td>
											<p class="collections-content">Jan 01, 2016 - 9:00 AM</p>
										</td>
										<td>
											<p class="collections-content">2 hours ago</p>
										</td>
									</tr>

									<tr>
										<td>4</td>
										<td>Juana Dela Cruz</td>
										<td>Jd Sarmiento</td>
										<td>
											<p class="collections-content">Jan 01, 2016 - 9:00 AM</p>
										</td>
										<td>
											<p class="collections-content">2 hours ago</p>
										</td>
									</tr>

									<tr>
										<td>5</td>
										<td>Juana Dela Cruz</td>
										<td>Jd Sarmiento</td>
										<td>
											<p class="collections-content">Jan 01, 2016 - 9:00 AM</p>
										</td>
										<td>
											<p class="collections-content">2 hours ago</p>
										</td>
									</tr>

									<tr>
										<td>6</td>
										<td>Juana Dela Cruz</td>
										<td>Jd Sarmiento</td>
										<td>
											<p class="collections-content">Jan 01, 2016 - 9:00 AM</p>
										</td>
										<td>
											<p class="collections-content">2 hours ago</p>
										</td>
									</tr>
									<!-- looping -->

								</tbody>
							</table>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div id="work-collections">
		<div class="row">
			<div class="col s12 m12 l6" id="nurseList">
				<h4 class="header">Latest Nurse</h4>
				<ul id="projects-collection" class="collection">

					<li class="collection-item avatar">
						<i class="mdi-social-people circle light-blue"></i>
						<span class="collection-header">Nurse</span>
						<p>Newly registered</p>
					</li>

					<li class="collection-item row-item">

						<div id="list-container">
							
							@if($nurse->count() == 0)
							<!-- NO DOCTOR YET REGISTERED -->
							<div class="hide empty-label" align="center">
								<h4 class="thin grey-text">NO NURSE FOUND</h4>
							</div>
							@else
							<!-- LIST Nurse -->
							<ul class="collection">
								@foreach($nurse as $n)
								<!--  LOOPING  -->
								<li class="collection-item nurse-item">
									<div class="row valign-wrapper">
										<div class="col m3 center hide-on-med-and-down">
											<img src="{{ asset('assets/images/avatars/img_parent.png') }}" alt="avatar" class="circle responsive-img z-depth-1 nurseDP"/>
										</div>
										<div class="col m5 s8">
											<span class="black-text">
												<p class="collections-title">{{$n->first_name.' '.$n->last_name}}</p>
												<p class="collections-content">{{date_format($n->created_at, 'F d Y')}}</p>
											 </span>
										</div>

										<div class="col m4 s4 center">
											<span class="black-text">
												<p class="collections-content">{{ ($n->active == 1) ? 'Active' : 'Inactivate' }}</p>
											 </span>
										</div>
									</div>
								</li>
								@endforeach
							</ul>
							@endif
						</div>
					</li>
				</ul>
			</div>

			<div class="col s12 m12 l6" id="patientList">
				<h4 class="header">Latest Patient</h4>
				<ul id="projects-collection" class="collection">

					<li class="collection-item avatar">
						<i class="mdi-social-people circle green"></i>
						<span class="collection-header">Patient</span>
						<p>Newly registered</p>
					</li>

					<li class="collection-item row-item">

						<div id="list-container">
							@if($patient->count() == 0)
							<!-- NO DOCTOR YET REGISTERED -->
							<div class="hide empty-label" align="center">
								<h4 class="thin grey-text">NO PATIENT FOUND</h4>
							</div>
							@else
							<!-- LIST DOCTOR -->
							<ul class="collection" style="margin: 0px;border: none;">
								@foreach($patient as $p)
								<!--  LOOPING  -->
								<li class="collection-item patient-item">
									<div class="row valign-wrapper">
										<div class="col m3 center hide-on-med-and-down">
											<img src="{{ asset('assets/images/avatars/img_parent.png') }}" alt="" class="circle responsive-img z-depth-1 patientDP">
										   
										</div>
										<div class="col m5 s8">
											<span class="black-text">
												<p class="collections-title">{{$p->first_name.' '.$p->last_name}}</p>
												<p class="collections-content">{{date_format($n->created_at, 'F d Y')}}</p>
											 </span>
										</div>

										<div class="col m4 s4 center">
											<span class="black-text">
												<p class="collections-content">{{ ($p->active == 1) ? 'Active' : 'Inactivate' }}</p>
											 </span>
										</div>
									</div>
								</li>
								@endforeach
							</ul>
							@endif
						</div>
					</li>
				</ul>
			</div>

		</div>
	</div>
</div>
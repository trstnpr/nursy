<div class="section">
	<p class="caption">Update Nurse {{$nurse->first_name}} {{$nurse->last_name}} general informations.</p>
	<div class="divider" id="divhr"></div>
	
	<!-- ERRORS -->
	<div class="card-panel white-text hide" id="div_errors">
		<ul class="error_list"></ul>
	</div>
	
	<div class="card-panel">
		<form class="col s12" id="accountForm">
			<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
			<h5>Name</h5>
			<div class="row">
				<div class="input-field col s12 l4">
					<input name="first_name" id="first_name" type="text" value="{{$nurse->first_name}}">
					<label for="first_name">First Name</label>
				</div>
				<div class="input-field col s12 l4">
					<input name="middle_name" id="middle_name" type="text" value="{{$nurse->middle_name}}">
					<label for="middle_name">Middle Name</label>
				</div>
				<div class="input-field col s12 l4">
					<input name="last_name" id="last_name" type="text" value="{{$nurse->last_name}}">
					<label for="last_name">Last Name</label>
				</div>
			</div>
			<h5>Status</h5>
			<div class="row">
				<div class="input-field col s12 l4">
					<select name="gender">
					  <option value="" selected>Choose your Gender</option>
					  <option value="Male" {{ ($nurse->gender == 'Male') ? 'selected' : '' }}>Male</option>
					  <option value="Female" {{ ($nurse->gender == 'Female') ? 'selected' : '' }}>Female</option>
					</select>
					<label>Select Gender</label>
				</div>

				<div class="input-field col s12 l4">
					<select name="civil_status">
					  <option value="" selected>What's your Status</option>
					  <option value="Single" {{ ($nurse->civil_status == 'Single') ? 'selected' : '' }}>Single</option>
					  <option value="Married" {{ ($nurse->civil_status == 'Married') ? 'selected' : '' }}>Married</option>
					  <option value="Divorced" {{ ($nurse->civil_status == 'Divorced') ? 'selected' : '' }}>Divorced</option>
					  <option value="Separated" {{ ($nurse->civil_status == 'Separated') ? 'selected' : '' }}>Separated</option>
					  <option value="Widowed" {{ ($nurse->civil_status == 'Widowed') ? 'selected' : '' }}>Widowed</option>
					</select>
					<label>Civil Status</label>
				</div>
		 
				<div class="input-field col s12 l4">
				  <input name="birthdate" type="date" value="{{$nurse->birthdate}}">
				  <label for="birthdate" class="active">Birthdate</label>
				  
				</div>
			</div>
			<h5> Complete Address</h5>
			<div class="row"> <!-- start row -->

				<div class="input-field col s12 l3">
					<input name="street" type="text" value="{{$nurse->street}}">
					<label for="street">House number, Street</label>
				</div>

				<div class="input-field col s12 l3">
					<input name="barangay" type="text" value="{{$nurse->barangay}}">
					<label for="barangay">Subdivision / Barangay</label>
				</div>

				<div class="input-field col s12 l3">
					<input name="town" type="text" value="{{$nurse->town}}">
					<label for="town">City / Town</label>
				</div>

				<div class="input-field col s12 l3">
					<input name="province" type="text" value="{{$nurse->province}}">
					<label for="province">Province</label>
				</div>

			</div> <!-- end row -->

			<h5> Educational Attainment </h5>
			<div class="row"> <!-- start row -->

				<div class="input-field col s12 l6">
					<input name="degree" type="text" value="{{$nurse->degree}}" required>
					<label for="degree">College Degree</label>
				</div>

				<div class="input-field col s12 l3">
					<input name="year_graduated" type="text" value="{{$nurse->year_graduated}}" required>
					<label for="year_graduated">Year Graduated</label>
				</div>

				<div class="input-field col s12 l3">
					<input name="school" type="text" value="{{$nurse->school}}" required>
					<label for="school">School</label>
				</div>

			</div> <!-- end row -->

			<h5> License No. </h5>
			<div class="row"> <!-- start row -->

				<div class="input-field col s12 l12">
					<input name="license" type="text" value="{{$nurse->license}}" required>
					<label for="license">PRC License # or <span class="red-text">TBD if not yet available</span></label>
				</div>

			</div> <!-- end row -->

			<h5> Contact Information</h5>
			<div class="row"> <!-- start row -->

				<div class="input-field col s12 l4">
					<input type="text" value="{{$nurse->email}}" disabled>
					<label for="street">Email Address (can't edit for now)</label>
				</div>

				<div class="input-field col s12 l4">
					<input name="cellphone" type="text" value="{{$nurse->cellphone}}">
					<label for="barangay">Cellphone Number</label>
				</div>

				<div class="input-field col s12 l4">
					<input name="landline" type="text" value="{{$nurse->landline}}">
					<label for="town">Landline Number</label>
				</div>
			</div> <!-- end row -->
			
			<br>
			<div class="row"> <!-- start row -->
				<div class="col s12 l12">
					<button type="submit" id="accountSubmit" class="btn btn-large waves-effect waves-cyan cyan"><i class="material-icons right">cloud</i> Save changes</button>
				</div>
			</div>
		</form>
	</div>
</div>
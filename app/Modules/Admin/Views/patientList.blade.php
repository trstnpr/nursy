@extends('admin.admin')

@section('content')
<!--breadcrumbs start-->
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
	<div class="row">
		<div class="col s12 m12 l12">
			<h5 class="breadcrumbs-title">Patient</h5>
			<ol class="breadcrumb">
				<li><a href="{{ url('admin/dashboard')}}">Dashboard</a>
				</li>
				<li class="active">Patient</li>
			</ol>
		</div>
	</div>
</div>
<!--breadcrumbs end-->

<!--start container s-->
<div class="container" id="user">
	<!-- CONTENT -->
	@include('Admin::includes.patientList')
</div>
@stop

@section('style')
<link rel="stylesheet" href="{{ asset('assets/css/dataTable/jquery.dataTables.min.css')}}">
@stop

@section('customScript')
<script type="text/javascript" src="{{ asset('assets/js/dataTable/jquery.dataTables.min.js')}}"></script>
<script>
	$('#patientList').dataTable();

	function updatePatient(user_id){
		window.location.href = "{{url('admin/updatePatient')}}/"+user_id;
	}

	function deletePatient(user_id){
		alertify.confirm("Delete this Patient", "Are you sure you want to delete this patient?", 
		function(){
			window.location.href = "{{url('admin/deletePatient')}}/"+user_id;
		}, function(){
			//CLOSE MODAL
		});
	}
</script>
@stop
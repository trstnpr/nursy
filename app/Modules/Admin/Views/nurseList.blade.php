@extends('admin.admin')

@section('content')
<!--breadcrumbs start-->
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
	<div class="row">
		<div class="col s12 m12 l12">
			<h5 class="breadcrumbs-title">Nurse</h5>
			<ol class="breadcrumb">
				<li><a href="{{ url('admin/dashboard')}}">Dashboard</a>
				</li>
				<li class="active">Nurse</li>
			</ol>
		</div>
	</div>
</div>
<!--breadcrumbs end-->

<!--start container s-->
<div class="container" id="user">
	<!-- CONTENT -->
	@include('Admin::includes.nurseList')
</div>
@stop

@section('style')
<link rel="stylesheet" href="{{ asset('assets/css/dataTable/jquery.dataTables.min.css')}}"
@stop

@section('customScript')
<script type="text/javascript" src="{{ asset('assets/js/dataTable/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript">
	$('#nurseList').dataTable();

	function changePublishStatus(user_id, status){
		var data = {
			'_token' : '{{ Session::token() }}',
			'id' : user_id,
			'publish' : status
		}
		$.ajax({
			url: "{{url('admin/changePublishStatus')}}", // Url to which the request is send
			type: "POST",             // Type of request to be send, called as method
			data: data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			cache: false,             // To unable request pages to be cached
			error: function(data){ // A function to be called if request failed
				alert("There's an error processing your request");
			},
			success: function(data){ // A function to be called if request succeeds
				var msg = JSON.parse(data);
				if(msg.result == 'success'){
					if(msg.publish == 1){
						$('.publish_btn'+msg.user_id).html('<button class="btn-flat green white-text" onClick="changePublishStatus('+msg.user_id+', 0);">PUBLISHED</button>');
					} else if(msg.publish == 0){
						$('.publish_btn'+msg.user_id).html('<button class="btn-flat red white-text" onClick="changePublishStatus('+msg.user_id+', 1);">UNPUBLISHED</button>');
					}
				} else{
					alertify.error("There's an error processing your request");
				}
			}
		});
	}

	function updateNurse(user_id){
		window.location.href = "{{url('admin/updateNurse')}}/"+user_id;
	}

	function deleteNurse(user_id){
		alertify.confirm("Delete this Nurse", "Are you sure you want to delete this nurse?", 
		function(){
			window.location.href = "{{url('admin/deleteNurse')}}/"+user_id;
		}, function(){
			//CLOSE MODAL
		});
	}
</script>
@stop
@extends('admin.admin')

@section('content')
<!--breadcrumbs start-->
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
	<div class="row">
		<div class="col s12 m12 l12">
			<h5 class="breadcrumbs-title">Password Management</h5>
			<ol class="breadcrumb">
				<li><a href="{{ url('admin/dashboard')}}">Dashboard</a>
				</li>
				<li class="active">Password Management</li>
			</ol>
		</div>
	</div>
</div>
<div class="section" style="padding:0 1.5em;">
	<div class="row">
		<p class="caption">Admin password management.</p>
		<div class="divider" id="divhr"></div>
		
		<!-- ERRORS -->
		<div class="card-panel white-text hide" id="div_errors">
			<ul class="error_list"></ul>
		</div>
		
		<div class="card-panel">
			<form id="passwordForm" method="POST">
				<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
				<h5>Your Current Password</h5>
				<div class="row"> <!-- start row -->
					<div class="input-field col s12 l12">
						<input name="current_password" type="password"></input>
						<label for="current_password">Current Password</label>
					</div>
				</div>
				
				<h5>Your New Password</h5>
				<div class="row"> <!-- start row -->
					<div class="input-field col s12 l6">
						<input name="password" type="password"></input>
						<label for="password">New Password</label>
					</div>
					<div class="input-field col s12 l6">
						<input name="password_confirmation" type="password"></input>
						<label for="password_confirmation">Confirm New Password</label>
					</div>
				</div>
				
				<div class="row"> <!-- start row s -->
					<div class="col s12 l12">
						<button type="submit" id="passwordSubmit" class="btn btn-large wave-effects wave-cyan cyan">
							<i class="material-icons right">cloud</i>Save Changes
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<!--breadcrumbs end-->

@stop

@section('customScript')
	<script type="text/javascript">
		$(document).ready(function(e){
			$("#passwordForm").on('submit',(function(e) {
				e.preventDefault();
				$.ajax({
					url: "{{url('admin/password/update')}}", // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					cache: false,             // To unable request pages to be cached
					processData:false,        // To send DOMDocument or non processed data file it is set to false
					beforeSend: function(){ $("#passwordSubmit").html('Saving...');},
					error: function(data){ // A function to be called if request failed
						if(data.readyState == 4){
							errors = JSON.parse(data.responseText);
							$('.error_list').empty();
							$.each(errors,function(key,value){
								$.each(value,function(k,val){
								$('.error_list').append("<li><strong class='valign-wrapper'><i class='mdi-navigation-close'></i> "+val+"</strong></li>");
								});
							});
							$("#div_errors").removeClass('hide green').addClass('red');
							$("html, body").animate({ scrollTop: 0 }, "slow");
							alertify.error('Oops! Something went wrong...');
						}
						$("#passwordSubmit").html('<i class="material-icons right">cloud</i>Save Changes');
					},
					success: function(data){ // A function to be called if request succeeds
						var msg = JSON.parse(data);
						if(msg.result == 'success'){
							$("html, body").animate({ scrollTop: 0 }, "slow");
							$("#div_errors").removeClass('red').addClass('hide');
							$("#passwordSubmit").html('<i class="material-icons right">cloud</i>Save Changes');
							alertify.success('Successfully Saved!');
						} else {
							$("html, body").animate({ scrollTop: 0 }, "slow");
							$("#div_errors").removeClass('hide green').addClass('red').html(msg.dialog);
							$("#passwordSubmit").html('<i class="material-icons right">cloud</i>Save Changes');
							alertify.error('Oops! Something went wrong...');
						}
					}
				});
			}));
		});
	</script>
@stop

<?php namespace App\Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model {

	/**
     * The database table.
     *
     */
    protected $table = 'admin';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['username', 'password'];

}
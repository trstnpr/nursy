<?php

Route::group(array('module' => 'Admin', 'middleware' => 'web', 'namespace' => 'App\Modules\Admin\Controllers'), function() {
	
	Route::get('/admin', 'AdminController@adminIndex');
	Route::get('/admin/dashboard', 'AdminController@dashBoard');
	Route::get('/admin/nurseList', 'AdminController@nurseList');
    Route::get('/admin/patientList', 'AdminController@patientList');
	Route::get('/admin/password', 'AdminController@passwordSettings');
	Route::get('/admin/logout', 'AdminController@adminLogout');
	Route::get('/admin/login', 'AdminController@adminLogin');
	Route::get('/admin/updateNurse/{id}', 'AdminController@updateNurse');
	Route::get('/admin/deleteNurse/{id}', 'AdminController@deleteNurse');
    Route::get('/admin/updatePatient/{id}', 'AdminController@updatePatient');
	Route::get('/admin/deletePatient/{id}', 'AdminController@deletePatient');
	Route::post('/admin/updateNurse/{id}/update', 'AdminController@updateNurseProcess');
    Route::post('/admin/updatePatient/{id}/update', 'AdminController@updatePatientProcess');
    Route::post('/admin/login', 'AdminController@adminLoginProcess');
	Route::post('/admin/changePublishStatus', 'AdminController@changePublishStatus');
	Route::post('/admin/password/update', 'AdminController@passwordUpdate');
});	
<?php namespace App\Modules\Admin\Controllers;

use Session;

use App\User;
use App\Modules\Admin\Models\Admin;
use App\Http\Requests;
use App\Http\Requests\PasswordRequest;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class AdminController extends Controller {
	
	public function adminIndex()
    {
        return redirect('admin/login');
    }

	public function adminLogin()
	{
		if(Session::get('admin_id')){
			return redirect('admin/dashboard');
		}

		return view("Admin::adminLogin");
	}

	public function adminLoginProcess(Request $req)
	{
		if(Session::get('admin_id')){
			return json_encode(array('result'=>'error', 'dialog'=>'dashboard'));
		}

		$result = Admin::where('username', '=', $req->username)->where('password', '=', md5($req->password))->first();
		if($result) {
			session()->put('admin_id', $result->id);
			return json_encode(array('result'=>'success', 'dialog'=>'dashboard'));
		} else {
			return json_encode(array('result'=>'error', 'dialog'=>'Invalid Credentials!'));
		}
	}

	public function adminLogout(){
		session()->forget('admin_id');
		return redirect('admin/login');
	} 

	public function dashBoard()
	{
		if(!Session::get('admin_id')){
			return redirect('admin/login');
		}

		$this->data['patient'] = User::where('role_id', '=', 3)->orderBy('created_at', 'desc')->get();
		$this->data['nurse'] = User::where('role_id', '=', 2)->orderBy('created_at', 'desc')->get();

		return view("Admin::dashboard", $this->data);
	}

	public function nurseList()
	{
		if(!Session::get('admin_id')){
			return redirect('admin/login');
		}

		$this->data['nurse'] = User::where('role_id', '=', 2)->get();
		return view("Admin::nurseList", $this->data);
	}

	public function updateNurse($id)
	{
		if(!Session::get('admin_id')){
			return redirect('admin/login');
		}

		$this->data['nurse'] = User::where('id', '=', $id)->first();
		return view("Admin::updateNurse", $this->data);
	}

	public function updateNurseProcess(Request $req, $id)
	{
		$input = $req->all();
		unset($input['_token']);
		foreach ($input as $key => $value) {
			if(empty($value)){
				$input[$key] = null;
			}
		}

		$result = User::where('id', '=', $id)->update($input);
		if($result){
			return json_encode(array('result'=>'success'));
		} else {
			return json_encode(array('result'=>'error', 'dialog'=>'There\'s an error updating your account'));
		}
	}

	public function deleteNurse($id)
	{
		if(!Session::get('admin_id')){
			return redirect('admin/login');
		}

		if(User::where('id', '=', $id)->delete()){
			return redirect("admin/nurseList");
		}
	}

	public function patientList()
	{
		if(!Session::get('admin_id')){
			return redirect('admin/login');
		}

		$this->data['patient'] = User::where('role_id', '=', 3)->get();
		return view("Admin::patientList", $this->data);
	}

	public function updatePatient($id)
	{
		if(!Session::get('admin_id')){
			return redirect('admin/login');
		}

		$this->data['patient'] = User::where('id', '=', $id)->first();
		return view("Admin::updatePatient", $this->data);
	}

	public function updatePatientProcess(Request $req, $id)
	{
		$input = $req->all();
		unset($input['_token']);
		foreach ($input as $key => $value) {
			if(empty($value)){
				$input[$key] = null;
			}
		}

		$result = User::where('id', '=', $id)->update($input);
		if($result){
			return json_encode(array('result'=>'success'));
		} else {
			return json_encode(array('result'=>'error', 'dialog'=>'There\'s an error updating your account'));
		}
	}

	public function deletePatient($id)
	{
		if(!Session::get('admin_id')){
			return redirect('admin/login');
		}

		if(User::where('id', '=', $id)->delete()){
			return redirect("admin/patientList");
		}
	}

	public function changePublishStatus(Request $req)
	{
		User::where('id', '=', $req->id)->update(['publish'=>$req->publish]);
		return json_encode(array('result'=>'success', 'publish'=>$req->publish, 'user_id'=>$req->id));
	}
	
	public function passwordSettings()
	{
		if(!Session::get('admin_id')){
			return redirect('admin/login');
		}
		return view("Admin::passwordManagement");
	}
	
	public function passwordUpdate(PasswordRequest $req)
	{
		//dd($req->all());
		$this->data['admin'] = Admin::where('id', '=', Session::get('admin_id'))->first();
		
		if(md5($req->current_password) == $this->data['admin']['password']) {
			$result = Admin::where('id', '=', $this->data['admin']['id'])->update(['password'=>md5($req->password)]);
			if($result){
				return json_encode(array('result'=>'success'));
			}

			return json_encode(array('result'=>'error', 'dialog'=>'There\'s an error updating your password'));
		} else {
			return json_encode(array('result'=>'error', 'dialog'=>'Your current password didn\'t matched!'));
		}
		
	}
	
}

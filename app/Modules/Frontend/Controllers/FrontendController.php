<?php namespace App\Modules\Frontend\Controllers;

use Auth;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mail;

use Illuminate\Http\Request;

class FrontendController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Auth::check()){
			$this->data['user'] = Auth::user();
		} else {
			$this->data['user'] = null;
		}
		return view("Frontend::index", $this->data);
	}

	public function contact()
	{
		if(Auth::check()){
			$this->data['user'] = Auth::user();
		} else {
			$this->data['user'] = null;
		}
		return view("Frontend::contact", $this->data);
	}

	public function contactSubmit(Request $req)
	{
		$input = $req->all();
		
		$contact_name = $input['contact_name'];
		$contact_email = $input['contact_email'];
		$contact_message = $input['contact_message'];
		$data = array('contact_name' => $contact_name, 'contact_email' => $contact_email, 'contact_message' => $contact_message);
		Mail::send('Frontend::contact-email-content', $data, function($message) use ($data){
			$message->to('info@incubixtech.com', 'Nursy Website')->subject('Contact Us - Nursy.co');
		});

		return json_encode(array('result'=>'success'));
	}

	public function activation(Request $req)
	{
		$email_code = $req->email_code;
		if($email_code == null || $email_code == ""){
			$this->data['message'] = 'error';
		} else {
			$result = User::where('email_activation_code', '=', $email_code)->update(['active'=> 1]);
			if($result){
				$this->data['message'] = 'success';
			} else {
				$this->data['message'] = 'error';
			}
		}

		return view("Frontend::email", $this->data);
	}

	public function emailActivate($data){
		Mail::send('Frontend::email-activation', $data, function($message) use ($data){
			$message->to($data['email'], $data['name'])->subject('Email Activation - Nursy.co');
		});
	}

}

<?php

Route::group(array('module' => 'Frontend', 'middleware' => 'web', 'namespace' => 'App\Modules\Frontend\Controllers'), function() {
	Route::get('/', 'FrontendController@index');
	Route::get('/activation', 'FrontendController@activation');
    Route::get('/contact', 'FrontendController@contact');
    Route::post('/contact/submit', 'FrontendController@contactSubmit');
});
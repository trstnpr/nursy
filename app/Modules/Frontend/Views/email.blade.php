@extends('layout-frontend.email')

@section('content')
<div id="error-page">

  <div class="row">
    <div class="col s12 l5 offset-l4">
      <div class="browser-window">
        <div class="top-bar">
          <div class="circles">
            <div id="close-circle" class="circle"></div>
            <div id="minimize-circle" class="circle"></div>
            <div id="maximize-circle" class="circle"></div>
          </div>
        </div>
        <div class="content">
          <div class="row">
            @if($message == 'success')
            <div id="site-layout-example-top" class="col s12">
              <p class="flat-text-logo center white-text caption-uppercase">thank you for joining us</p>
            </div>
            <div id="site-layout-example-right" class="col s12 m12 l12">
              <div class="row center">
                <h1 class="text-long-shadow col s12" style="font-size: 5em;">Account Activated!</h1>
                <a href="{{url('/login')}}" class="btn">LOGIN NOW!</a>
              </div>
            </div>
            @else
            <div id="site-layout-example-top" class="col s12">
              <p class="flat-text-logo center white-text caption-uppercase">Oops... there's something wrong in here!</p>
            </div>
            <div id="site-layout-example-right" class="col s12 m12 l12">
              <div class="row center">
                <h1 class="text-long-shadow col s12" style="font-size: 3em;">There's an error activating your account!</h1>
                <a href="{{url('/')}}" class="btn">GO TO MAIN PAGE</a>
              </div>
            </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@stop

@section('style')
<style type="text/css">
    body {
      padding-top: 10%;
    }
</style>
@stop

@section('customScript')
<script type="text/javascript">
  
</script>
@stop
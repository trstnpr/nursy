@extends('layout-frontend.frontend')

@section('content')
<section id="frontend">
	@include('Frontend::includes.content-1')
	@include('Frontend::includes.content-2')
	@include('Frontend::includes.content-3')
	@include('Frontend::includes.content-4')
	@include('Frontend::includes.content-5')
	@include('Frontend::includes.content-6')
</section>
@stop

@section('style')
<style type="text/css">
	.card-content {
		height: 200px;
	}
	.home-services li {
		text-align:left;
	}
</style>
@stop

@section('customScript')
<script type="text/javascript">
	// Modal
	$(document).ready(function() {
		$('.modal-trigger').leanModal();
	});
</script>
@stop
<div id="content-6" class="content-6">
	
	<div class="container">

		<div class="row">

			<div class="col s12 m12 l12" align="center">
				<h4><black>Get Started</black></h4>
				<h5 class="h5">Before anything else, let's move on and get it started!</h5>
			</div>

		</div>

		<div class="row">
			
			<div class="col s12 l6 offset-l3">
				
				<div class="row">

					<div class="col s12 m6 l6" align="center">
						<a href="#" class="btn btn-radius btn-flat btn-large btn-block grey darken-3 white-text" style="opacity: 0.7;">Patient</a>
					</div>

					<div class="col s12 m6 l6" align="center">
						<a href="#" class="btn btn-radius btn-flat btn-large btn-block  light-blue  lighten-1 white-text">Nurse</a>
					</div>

				</div>

			</div>

		</div>

	</div>

</div>
<div id="content-5" class="content-5">

	<div class="container">

		<div class="row">
			
			<div class="col s12 m12 l12">
				<h4 class="center white-text">Features</h4>
				<h5 class="center h5 white-text">We tried to make Nursy as easy as possible to use and we also added a few additional<br/>features that adds value to both nurses, caretakers and patients.</h5>
			</div>

		</div>


				
		<div class="row">
	
			<div class="col s12 m5 l5 padding-left" align="right">
				<div>
					<h4 class="h4 medium white-text text-darken-2">Messaging</h4>
					<p class="white-text text-darken-1">Essential communication tool between patients and nurses for questions and updates.</p>
				</div>
			</div>

			<div class="col s12 m2 l2 padding-top half">
				<span class="icon-circle left"><i class="mdi-communication-forum"></i></span>
				<span class="icon-circle right"><i class="mdi-editor-format-list-bulleted"></i></span>
			</div>

			<div class="col s12 m5 l5 padding-right" align="left">
				<h4 class="h4 medium white-text text-darken-2">Online Patient History</h4>
				<p class="white-text text-darken-1">Patients can review and track their historical data online. Nurses can update records in real-time.</p>
			</div>

		</div>
		
		<div class="row">
	
			<div class="col s12 m5 l5 padding-left" align="right">
				<div>
					<h4 class="h4 medium white-text text-darken-2">Reminders</h4>
					<p class="white-text text-darken-1">Reminders are sent to parties prior to the appointment.</p>
				</div>
			</div>

			<div class="col s12 m2 l2 padding-top half">
				<span class="icon-circle left"><i class="mdi-av-timer"></i></span>
				<span class="icon-circle right"><i class="mdi-action-book"></i></span>
			</div>

			<div class="col s12 m5 l5 padding-right" align="left">
				<h4 class="h4 medium white-text text-darken-2">Online Booking System</h4>
				<p class="white-text text-darken-1">Manage all appointments from one single booking system.</p>
			</div>

		</div>


	</div>

	<!-- end -->
	 
	

</div>
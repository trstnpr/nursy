<div id="content-4" class="content-4">

	<div class="container">

		<div class="row">

			<div class="col s12 m12 l10 offset-l1 center">
				<h4>How it Works</h4>
				<h5 class="h5">for Patients</h5>
				<img class="responsive-img materialboxed" src="{{ asset('assets/images/material/how_it_works_patientC.png') }}" style="margin-top: 5em;">
				<br/>
				<p class="h5">All nurses have undergone a thorough background check and accreditation of their credentials. Following the visit the nurse will complete a full online report of the treatment and state of the patient (Nurse’s Endorsement). This log will provide the patient valuable historic data.</p>
			</div>

		</div>

	</div>


</div>
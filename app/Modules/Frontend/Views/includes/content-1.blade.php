<div id="content-1" class="content-1">
	
	<!-- container -->
	<div class="container">
		<div class="row">
			<div class="col s12 m6 l6">
				<div class="row">
					<div class="col s12 m5 l5">
						<!-- logo -->
						<img src="{{ asset('assets/images/logo/nursy-logo-2.png') }}" class="responsive-img">
					</div>
				</div>

				<div class="row">
					<div class="col s12 m12 l12">
						<!-- headings -->
						<h4 class="thin white-text">Healthcare at Your Doorstep</h4>
						<h5 class="white-text h5">We provide easy access to nursing care wherever you are - on demand round the clock.</h5>
					</div>
				</div>

				<div class="row">
					<div class="col s12 m5 l5">
						<button class="btn btn-flat btn-large btn-block btn-transparent white-text waves-effect waves-light" id="learn_more"> Learn more </button>
					</div>
					<div class="col s12 m7 l7">
						<a href="{{ url('/register') }}" class="btn btn-flat white btn-large btn-radius btn-block cyan-text waves-effect waves-dark"id="sign_free"> SIGN UP FOR FREE</a>
					</div>
				</div>
			</div>

			<div class="col s12 m6 l6 ">
				<!-- MACBOOK -->
				<!--img src="{{ asset('assets/images/material/mac.png') }}" class="responsive-img materialboxed" /-->
			</div>

		</div>

	</div>
</div>
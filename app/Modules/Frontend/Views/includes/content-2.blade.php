<div id="content-2" class="content-2">
	<div class="container">

		<div class="row">
			<div class="col s12 m12 l12">
				<h4 class="center">Services</h4>
				<h5 class="h5 grey-text center">At Nursy we provide a one stop service for all nursing and caretaking related matter.<br/>Please have a look at our services listed below</h5>
			</div>
		</div>

		<div class="row">
			
			<div class="col s12 m12 l12">
				
				<div class="row">
			
					<div class="col s12 m3 l3">
						<div class="card">
							<div class="card-image" style="border-top:6px solid #28ade3;">
								<div class="images" style="background-image: url('{{ asset('assets/images/material/1.jpg') }}')"></div>
							</div>
							<div class="card-content center">
								<h5 class="h5 grey-text text-darken-4 bold">CAREGIVER<br/><small>(Shift Based)</small></h5>
								<p>Nursing care for assisted daily living in the absence of significant other or family member.</p>
							</div>
							<div class="card-action center">
								<a class="btn btn-flat waves-effect waves-light cyan modal-trigger white-text" href="#services_modal">See Details</a>
							</div>
						</div>
					</div>

					<div class="col s12 m3 l3">
						<div class="card">
							<div class="card-image" style="border-top:6px solid #28ade3;">
								<div class="images" style="background-image: url('{{ asset('assets/images/material/2.jpg') }}')"></div>
							</div>
							<div class="card-content center">
								<h5 class="h5 grey-text text-darken-4 bold">PRIVATE DUTY NURSE<br/><small>(Shift Based)</small></h5>
								<p>Nursing care for patients in and out of hospital, and patients needing special nursing attention.</p>
							</div>
							<div class="card-action center">
								<a class="btn btn-flat waves-effect waves-light cyan modal-trigger white-text" href="#services_modal">See Details</a>
							</div>
						</div>
					</div>

					<div class="col s12 m3 l3">
						<div class="card">
							<div class="card-image" style="border-top:6px solid #28ade3;">
								<div class="images" style="background-image: url('{{ asset('assets/images/material/3.jpg') }}')"></div>
							</div>
							<div class="card-content center">
								<h5 class="h5 grey-text text-darken-4 bold">HOME VISIT NURSE<br/><small>(Visit)</small></h5>
								<p>Brief nurse visit needing focused care from health monitoring to assessment and carrying out physician's order.</p>
							</div>
							<div class="card-action center">
								<a class="btn btn-flat waves-effect waves-light cyan modal-trigger white-text" href="#services_modal">See Details</a>
							</div>
						</div>
					</div>

					<div class="col s12 m3 l3">
						<div class="card">
							<div class="card-image" style="border-top:6px solid #28ade3;">
								<div class="images" style="background-image: url('{{ asset('assets/images/material/4.jpg') }}')"></div>
							</div>
							<div class="card-content center">
								<h5 class="h5 grey-text text-darken-4 bold">SPECIAL CARE NURSING<br/><small>(Shift Based)</small></h5>
								<p>Specialised nursing care for patients needing critical care nursing such as Cardiac Monitoring Management & Peritoneal Dialysis Change.</p>
							</div>
							<div class="card-action center">
								<a class="btn btn-flat waves-effect waves-light cyan modal-trigger white-text" href="#services_modal">See Details</a>
							</div>
						</div>
					</div>

				</div>

			</div>

		</div>

	</div>
	
</div>

<style>
	.services #list_services {
		text-align:left;
	}
</style>

<!-- #services modal -->
<div id="services_modal" class="modal">
	<div class="modal-content" style="border-top:6px solid #28ade3;">
		<table class="striped services">
			<thead>
				<tr>
					<th id="list_services">LIST OF SERVICES</th>
					<th>Caregiver with Nurse Supervission</th>
					<th>Private Duty Nurse</th>
					<th>Home Visit Nurse</th>
					<th>Special Care Nurse</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td id="list_services">
						<b>Online Report</b>
						<ul class="fa-ul">
							<li><i class="fa fa-li fa-plus-square green-text"></i> Endorsement (Complete online nursing report)</li>
						</ul>
					</td>
					<td><i class="fa fa-check-circle fa-2x green-text"></i></td>
					<td><i class="fa fa-check-circle fa-2x green-text"></i></td>
					<td><i class="fa fa-check-circle fa-2x green-text"></i></td>
					<td><i class="fa fa-check-circle fa-2x green-text"></i></td>
				</tr>
				<tr>
					<td id="list_services">
						<b>Bebside Care</b>
						<ul class="fa-ul">
							<li><i class="fa fa-li fa-plus-square green-text"></i> Bed bath / Hygeine / Grooming</li>
							<li><i class="fa fa-li fa-plus-square green-text"></i> Turning and range of motion (ROM)</li>
						</ul>
					</td>
					<td><i class="fa fa-check-circle fa-2x green-text"></i></td>
					<td><i class="fa fa-check-circle fa-2x green-text"></i></td>
					<td><i class="fa fa-check-circle fa-2x green-text"></i></td>
					<td><i class="fa fa-check-circle fa-2x green-text"></i></td>
				</tr>
				<tr>
					<td id="list_services">
						<b>General Care</b>
						<ul class="fa-ul">
							<li><i class="fa fa-li fa-plus-square green-text"></i> Vital signs monitoring</li>
							<li><i class="fa fa-li fa-plus-square green-text"></i> Glucose level monitoring</li>
							<li><i class="fa fa-li fa-plus-square green-text"></i> Post operation monitoring and telehealth and health teaching</li>
							<li><i class="fa fa-li fa-plus-square green-text"></i> Wound care, foot and dressing changes</li>
							<li><i class="fa fa-li fa-plus-square green-text"></i> Ostomy/colostomy teaching and management</li>
							<li><i class="fa fa-li fa-plus-square green-text"></i> Medication reconciliation and reports to doctors or pharmacists</li>
							<li><i class="fa fa-li fa-plus-square green-text"></i> Medication administration</li>
						</ul>
					</td>
					<td><i class="fa fa-check-circle fa-2x green-text"></i></td>
					<td><i class="fa fa-check-circle fa-2x green-text"></i></td>
					<td><i class="fa fa-check-circle fa-2x green-text"></i></td>
					<td><i class="fa fa-check-circle fa-2x green-text"></i></td>
				</tr>
				<tr>
					<td id="list_services">
						<b>Skilled Nursing Care</b>
						<ul class="fa-ul">
							<li><i class="fa fa-li fa-plus-square green-text"></i> Injections</li>
							<li><i class="fa fa-li fa-plus-square green-text"></i> Heparin flushes</li>
							<li><i class="fa fa-li fa-plus-square green-text"></i> Ostomy/colostomy teaching and management</li>
							<li><i class="fa fa-li fa-plus-square green-text"></i> Foley Catheter care</li>
							<li><i class="fa fa-li fa-plus-square green-text"></i> IV management</li>
						</ul>
					</td>
					<td></td>
					<td><i class="fa fa-check-circle fa-2x green-text"></i></td>
					<td><i class="fa fa-check-circle fa-2x green-text"></i></td>
					<td><i class="fa fa-check-circle fa-2x green-text"></i></td>
				</tr>
				<tr>
					<td id="list_services">
						<b>Special Care</b>
						<ul class="fa-ul">
							<li><i class="fa fa-li fa-plus-square green-text"></i> Cardiac Monitoring Management</li>
							<li><i class="fa fa-li fa-plus-square green-text"></i> Peritoneal Dialysis change</li>
						</ul>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td><i class="fa fa-check-circle fa-2x green-text"></i></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="modal-footer blue">
		<a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat white-text">Close</a>
	</div>
</div>
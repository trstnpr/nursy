<div id="content-3" class="content-3">
	
	<div class="container">

		<div class="row">

			<div class="col s12 m12 l10 offset-l1 center">
				<h4 class="white-text">How it Works</h4>
				<h5 class="h5 white-text">for Nurses</h5>
				<img class="responsive-img materialboxed" src="{{ asset('assets/images/material/how_it_works_nurseW.png') }}" style="margin-top: 5em;">
				<br/>
				<p class="h5 white-text">All nurses will rate the experience to ensure high standards of working conditions. Following a visit the nurse is required to give an endorsement including a complete report of the visit, all vital signs of the patient and treatment given.</p>
			</div>

		</div>

	</div>

</div>
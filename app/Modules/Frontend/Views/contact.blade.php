@extends('layout-frontend.contact')

@section('content')
<section id="contact">
    <div class="card-panel z-depth-0 blue-grey darken-2" style="margin-top: -10px;" align="center">
        <h3 class="thin white-text">Contact Us</h3>
    </div>
    <br>
    <div class="container">
        <div class="row">
          <!-- Information -->
          <div class="col s12 m12 l5">

            <address class="blue-grey-text text-darken-3">
              28th Floor Pacific Star Bldg.,
              Makati Ave. corner Buendia Ave., Makati City,
              Philippines
              <br>
              <br>
               +(632) 822 - 2755
              <br>
              <br>
              info@nursy.co
              <br>
              <br>
              <a href="#" class="blue-grey-text text-darken-3" style="margin: 10px;"><i class="fa fa-facebook fa-lg"></i></a>
              <a href="#" class="blue-grey-text text-darken-3" style="margin: 10px;"><i class="fa fa-twitter fa-lg"></i></a>
            </address>
             <!--  google map -->
            <br>
            <div id="gmap_canvas" class="circle" style="height:275px;width:275px;margin:0 auto;"></div>
            <br>
          </div>
          
          <div class="col s12 m12 l7">
              <div class="row card-panel blue-grey darken-2" style="color:#FFF;">
                  <form method="POST" id="contactForm">
                      {!! csrf_field() !!}
                      <div class="input-field col s12">
                        <input type="text" name="contact_name" required="required" placeholder="Your name" required="required"/>
                        <label for="name">Name</label>
                      </div>

                      <div class="input-field col s12">
                        <input class="validate" type="email" name="contact_email" required="required" placeholder="Your email" />
                        <label for="email" data-error="wrong" data-success="right">Email</label>
                      </div>

                      <div class="input-field col s12">
                        <textarea name="contact_message" class="materialize-textarea" required="required" placeholder="Your message ..."></textarea>
                        <label for="textarea1">Message</label>
                      </div>

                      <!-- error message -->
                      <div class="input-field col s12 hide" id="error_div">
                          <div class="card-panel red ligthen-2 white-text" id="error_msg">
                              <strong><i class="mdi-alert-warning"></i> Error Message</strong>
                              <ul class="error_list">
                              </ul>
                          </div>
                      </div>

                      <div class="input-field col s12 card-action">
                          <div class="g-recaptcha" data-sitekey="6LfPSh0TAAAAAIs9Q9yC9OjBm94wNYyAb5U70lnl"></div>

                          <br>

                          <button type="submit" id="contactSubmit" class="btn btn-large blue-grey waves-effect waves-light">Submit</button>
                      </div>
                  </form>
              </div>
          </div>


        </div>
    </div>
    <br>
</section>

@stop

@section('style')
<style type="text/css">
    #gmap_canvas {
      border: 5px solid #eee;
    }
    #gmap_canvas img{max-width:none !important;background:none !important; }
</style>
@stop

@section('customScript')
<script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>
<script type="text/javascript">
    function init_map(){
      var myOptions = {
          zoom:16,
          center:new google.maps.LatLng(14.5610136,121.02696990000004),
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDefaultUI: true
        };

        map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
        marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(14.5610136,121.02696990000004)});

        google.maps.event.addListener(marker, 'click', function(){
          infowindow.open(map,marker);
        });

        //infowindow.open(map,marker);
    }
    google.maps.event.addDomListener(window, 'load', init_map);
</script>

<!-- GOOGLE RECAPTCHA -->
<script src='https://www.google.com/recaptcha/api.js'></script>

<script type="text/javascript">
    $(document).ready(function(e) {
      $("#contactForm").on('submit',(function(e) {
        e.preventDefault();

          var response = grecaptcha.getResponse();

          if(response === ''){
              alert('Check the Captcha first!');
              return false;
          }

        $.ajax({
          url: "{{url('contact/submit')}}", // Url to which the request is send
          type: "POST",             // Type of request to be send, called as method
          data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
          contentType: false,       // The content type used when sending data to the server.
          cache: false,             // To unable request pages to be cached
          processData:false,        // To send DOMDocument or non processed data file it is set to false
          beforeSend: function(){ $("#contactSubmit").html('Submitting...');},
          error: function(data){ // A function to be called if request failed
            if(data.readyState == 4){
              errors = JSON.parse(data.responseText);
              $('.error_list').empty();
              $.each(errors,function(key,value){
                $.each(value,function(k,val){
                $('.error_list').append("<li><strong class='valign-wrapper'><i class='mdi-navigation-close'></i> "+val+"</strong></li>");
                });
              });
              $("#error_msg").removeClass('green').addClass('red');
              $('#error_div').removeClass('hide');
            }
            $("#contactSubmit").html('Submit');
          },
          success: function(data){ // A function to be called if request succeeds
            var msg = JSON.parse(data);
            if(msg.result == 'success'){
              $("#contactSubmit").html('Submit');
              $("#error_msg").removeClass('red').addClass('green').html("<strong><i class='mdi-navigation-check'></i> Message Sent! </strong>");
              $("#error_div").removeClass('hide');
            } else{
              $("#contactSubmit").html('Submit');
              $("#error_msg").removeClass('green').addClass('red').html("<strong class='valign-wrapper'><i class='mdi-navigation-close'></i> "+msg.dialog+" </strong>");
              $("#error_div").removeClass('hide');
            }
          }
        });
      }));
    });
</script>
@stop
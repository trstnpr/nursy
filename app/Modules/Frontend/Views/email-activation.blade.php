<html>
  <body style="background-color:#00bcd4;padding-top:10%;">
    <center>
    <div style="width:700px;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;">
      <div style="background: linear-gradient(#e7e7e6, #E2E2E1);padding-top: 10px;		padding-bottom: 30px;padding-left: 0px;padding-right: 0px; border-top-right-radius: 5px;		border-top-left-radius: 5px;">

           <ul style="list-style: none;">
             <li style="width:15px; height:15px;border-radius: 50%;float: left;margin-left: 3px;margin-right:3px;background-color:#1BC656;"></li>

             <li style="width:15px; height:15px;border-radius: 50%;float: left;		margin-left: 3px;margin-right: 3px;background-color:#FF5C5A;"></li>

             <li style="width:15px; height:15px;		border-radius: 50%;float: left;margin-left: 3px;margin-right: 3px;background-color:#FFBB50;"></li>
           </ul>
      </div>
      <div style="padding: 10px 10px 10px 10px;		background-color:#E57373;font-size:1.5em;color:#fff;font-weight:100;">
        Email Activation - Nursy.co
      </div>
      <div style="padding: 40px;background-color:#26a69a;height:auto;color:#fff;" align="left">
          <h3>Hello {{$name}},</h3>
          <h4 style="line-height:150%;">You're successfully registered to Nursy. You must activate your account before logging in. To activate your account, please click the link below:</h4>
          <center>
            <a href="http://www.nursy.co/activation/?email_code={{$email_activation_code}}" style="text-decoration:none;color:#fff;background-color:#607d8b ;display:block;padding:1em;text-align:center;border-radius:5px;"><strong>Activate your account</strong></a>
          <br/>
          <h4 style="line-height:150%;">Or copy and paste this link to your browser: </h4>
          <p style="background-color:#ccc;display:block;padding:1em;text-align:center;border-radius:5px;color:#222;">http://www.nursy.co/activation/?email_code={{$email_activation_code}}</p>
          <br/>
            &copy; <a href='http://www.nursy.co' style='color:#fff;text-decoration:none;letter-spacing:1px;display:inline-block;'>nursy.co</a>
        </center>
      </div>
    </div>
    </center>
  </body>
</html>
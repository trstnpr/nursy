<html>
  <body>
    <center>
    <div style="width:700px;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;">
      <div style="background: linear-gradient(#e7e7e6, #E2E2E1);padding-top: 10px;		padding-bottom: 30px;padding-left: 0px;padding-right: 0px; border-top-right-radius: 5px;		border-top-left-radius: 5px;">

           <ul style="list-style: none;">
             <li style="width:15px; height:15px;border-radius: 50%;float: left;margin-left: 3px;margin-right:3px;background-color:#1BC656;"></li>

             <li style="width:15px; height:15px;border-radius: 50%;float: left;		margin-left: 3px;margin-right: 3px;background-color:#FF5C5A;"></li>

             <li style="width:15px; height:15px;		border-radius: 50%;float: left;margin-left: 3px;margin-right: 3px;background-color:#FFBB50;"></li>
           </ul>
      </div>
      <div style="padding: 10px 10px 10px 10px;		background-color:#E57373;font-size:1.5em;color:#fff;font-weight:100;">
        Message from Contact Us Page - Nursy.co
      </div>
      <div style="padding: 40px;background-color:#26a69a;height:auto;color:#fff;" align="left">
            <table>
            <tr>
              <td style="padding:.5em 0;"><strong>Name</strong></td>
              <td><strong>: {{$contact_name}}</strong></td>
            </tr>
            <tr>
              <td style="padding:.5em 0;"><strong>Email</strong></td>
              <td><strong>: {{$contact_email}}</strong></td>
            </tr>
            <tr>
              <td style="padding:.5em 0;"><strong>Message</strong></td>
              <td><strong>: {{$contact_message}}</strong></td>
            </tr>
          </table>
          <br/>
        <center>
            &copy; <a href='http://www.nursy.co' style='color:#fff;text-decoration:none;letter-spacing:1px;display:inline-block;'>nursy.co</a>
        </center>
      </div>
    </div>
    </center>
  </body>
</html>
<?php namespace App\Modules\Nurse\Models;

use Illuminate\Database\Eloquent\Model;

class Nurse extends Model {

	/**
     * The database table.
     *
     */
    protected $table = 'users';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'image', 'first_name', 'middle_name', 'last_name', 'gender', 'civil_status', 'birthdate', 'street', 'barangay', 'town', 'province', 'degree', 'year_graduated', 'school', 'license', 'about', 'background', 'cellphone', 'landline', 'email', 'nurse_type', 'services', 'password', 'email_activation_code', 'active', 'role_id', 'password_recover'
    ];

}

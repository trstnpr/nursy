<?php namespace App\Modules\Nurse\Models;

use Illuminate\Database\Eloquent\Model;

class Services extends Model {

	/**
     * The database table.
     *
     */
    protected $table = 'services';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['service'];

}

<?php namespace App\Modules\Nurse\Controllers;

use Auth;
use Hash;
//REQUESTS
use App\Http\Requests;
use App\Http\Requests\NurseAccountRequest;
use App\Http\Requests\PasswordRequest;
use App\Http\Requests\ScheduleRequest;
use App\Http\Requests\ServicesRequest;
//MODELS
use App\Models\Province;
use App\Modules\Nurse\Models\Nurse;
use App\Models\NurseType;
use App\Models\Services;
use App\Modules\Patient\Models\Patient;
use App\Modules\Schedule\Models\Schedule;
//CONTROLLERS
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class NurseController extends Controller {

	public function __construct(){
		$this->data['user'] = Auth::user();
	}

	public function profileSettings()
	{
		return view("Nurse::profile", $this->data);
	}

	public function profileSettingsUpdate(Request $req)
	{
		$input = $req->all();
		unset($input['_token']);
		foreach ($input as $key => $value) {
			if(empty($value)){
				$input[$key] = null;
			}
		}

		$result = Nurse::where('id', '=', Auth::user()->id)->update($input);
		if($result){
			return json_encode(array('result'=>'success'));
		} else {
			return json_encode(array('result'=>'error', 'dialog'=>'There\'s an error updating your account'));
		}
	}

	public function accountSettings()
	{
		$this->data['provinces'] = Province::all();
		return view("Nurse::account", $this->data);
	}

	public function accountSettingsUpdate(NurseAccountRequest $req)
	{
		$input = $req->all();
		unset($input['_token']);
		foreach ($input as $key => $value) {
			if(empty($value)){
				$input[$key] = null;
			}
		}

		$result = Nurse::where('id', '=', Auth::user()->id)->update($input);
		if($result){
			return json_encode(array('result'=>'success'));
		} else {
			return json_encode(array('result'=>'error', 'dialog'=>'There\'s an error updating your account'));
		}
	}

	public function passwordSettings()
	{
		return view("Nurse::password", $this->data);
	}

	public function passwordSettingsUpdate(PasswordRequest $req)
	{
		if(Hash::check($req->current_password, Auth::user()->password)){
			
			$input['password'] = Hash::make($req->password);

			Nurse::where(array('id' => Auth::user()->id))->update($input);

			return json_encode(array('result'=>'success'));

		} else{
			return json_encode(array('result'=>'error', 'dialog'=>'Old Password does not match!'));
		}
	}

	//dashboard
	public function dashBoard()
	{
		$this->data['appointments'] = Schedule::where('nurse_id', '=', Auth::user()->id)->where('reserve', '=', 1)->orderBy('date_start', 'desc')->get();
		//dd($this->data['appointments']);
		foreach ($this->data['appointments'] as $key => $value) {
			$value['patient'] = Patient::where('id', '=', $value->patient_id)->first();
		}
		return view("Nurse::dashBoard", $this->data);
	}
	
	//schedule
	public function schedule()
	{
		$this->data['schedule'] = Schedule::where('nurse_id', '=', Auth::user()->id)->where('date_start', '>=', date('Y-m-d H:i:s'))->orderBy('date_start', 'desc')->get();
		return view("Nurse::schedule", $this->data);
	}

	//add schedule
	public function addSchedule()
	{
		return view("Nurse::addSchedule", $this->data);
	}

	public function addScheduleProcess(ScheduleRequest $req)
	{
		$date = date_format(date_create($req->date), 'Y-m-d');
		$start = date_format(date_create($req->time_start), 'H:i:s');
		$end = date_format(date_create($req->time_end), 'H:i:s');
		
		$input['nurse_id'] = Auth::user()->id;
		$input['date_start'] = $date.' '.$start;
		$input['date_end'] = $date.' '.$end;

		$scheduleExist = Schedule::where('nurse_id', '=', $input['nurse_id'])->where('date_start', '=', $input['date_start'])->count();
		
		if($scheduleExist > 0)
			return json_encode(array('result'=>'error', 'dialog'=>'You have this schedule already.'));
		else {
			$result = Schedule::create($input);
			return json_encode(array('result'=>'success'));
		}

	}

	public function editSchedule($id)
	{
		if(Schedule::where('id', '=', $id)->where('nurse_id', '=', Auth::user()->id)->count() == 0){
			return redirect("nurse/schedule");
		}

		$this->data['schedule'] = Schedule::where('id', '=', $id)->first();

		return view("Nurse::editSchedule", $this->data);
	}

	public function editScheduleProcess(ScheduleRequest $req)
	{
		$sched_id = $req->sched_id;
		$date = date_format(date_create($req->date), 'Y-m-d');
		$start = date_format(date_create($req->time_start), 'H:i:s');
		$end = date_format(date_create($req->time_end), 'H:i:s');
		
		$input['nurse_id'] = Auth::user()->id;
		$input['date_start'] = $date.' '.$start;
		$input['date_end'] = $date.' '.$end;

		$scheduleExist = Schedule::where('id', '!=', $sched_id)->where('nurse_id', '=', $input['nurse_id'])->where('date_start', '=', $input['date_start'])->count();
		
		if($scheduleExist > 0)
			return json_encode(array('result'=>'error', 'dialog'=>'You have this schedule already.'));
		else {
			$result = Schedule::where('id', '=', $sched_id)->where('nurse_id', '=', Auth::user()->id)->update($input);
			if($result){
				return json_encode(array('result'=>'success'));
			} else {
				return json_encode(array('result'=>'error', 'dialog'=>'I think you\'ve just edit the code.'));
			}
		}

	}

	public function deleteScheduleProcess($id){
		if(Schedule::where('id', '=', $id)->where('nurse_id', '=', Auth::user()->id)->count() == 1){
			Schedule::where('id', '=', $id)->where('nurse_id', '=', Auth::user()->id)->delete();

			return json_encode(array('result'=>'success', 'dialog'=>'Schedule Deleted!'));
		} else {
			return json_encode(array('result'=>'error', 'dialog'=>'There\'s an error deleting your schedule!'));
		}

	}
	
	// services
	public function services()
	{
		$this->data['nurse_type'] = NurseType::all();
		$this->data['nurse_services'] = Services::all();
		return view("Nurse::services", $this->data);
	}
	public function updateServices(ServicesRequest $req) {
		$nurse_type = $req->nurseType;
		$services = implode(' | ', $req->services);
		$result = Nurse::where('id', '=', Auth::user()->id)->update(['nurse_type'=>$nurse_type, 'services'=>$services]);
		if($result){
			return json_encode(array('result'=>'success'));
		} else {
			return json_encode(array('result'=>'error', 'dialog'=>'Oops! Something went wrong.'));
		}
	}
	
	// DHTMLX
	public function dhtmlx()
	{
		return view("Nurse::dhtmlx", $this->data);
	}
	
	//appointment
	public function appointment()
	{
		$this->data['appointments']['present'] = Schedule::where('nurse_id', '=', Auth::user()->id)->where('date_end', '>=', date('Y-m-d H:i:s'))->where('reserve', '=', 1)->get();
		$this->data['appointments']['past'] = Schedule::where('nurse_id', '=', Auth::user()->id)->where('date_end', '<', date('Y-m-d H:i:s'))->where('reserve', '=', 1)->get();
		//dd($this->data['appointments']);
		foreach($this->data['appointments']['present'] as $key=>$value){
			$value['patient'] = Patient::where('id', '=', $value['patient_id'])->first();
		}
		foreach($this->data['appointments']['past'] as $key=>$value){
			$value['patient'] = Patient::where('id', '=', $value['patient_id'])->first();
		}
		return view("Nurse::appointment", $this->data);
	}
}

<?php

Route::group(array('module' => 'Nurse', 'middleware' => ['web', 'auth', 'nurse_role'], 'namespace' => 'App\Modules\Nurse\Controllers'), function() {
	Route::get('nurse/userDashBoard', 'NurseController@dashBoard');
	Route::get('nurse/services', 'NurseController@services');
	Route::get('nurse/appointment', 'NurseController@appointment');
	Route::get('nurse/schedule', 'NurseController@schedule');
	Route::get('nurse/schedule/addSchedule', 'NurseController@addSchedule');
    Route::post('nurse/schedule/addSchedule/process', 'NurseController@addScheduleProcess');
    Route::get('nurse/schedule/editSchedule/{id}', 'NurseController@editSchedule');
    Route::post('nurse/schedule/editSchedule/process', 'NurseController@editScheduleProcess');
	Route::get('nurse/profileSettings', 'NurseController@profileSettings');
	Route::post('nurse/profileSettings/update', 'NurseController@profileSettingsUpdate');
	Route::get('nurse/accountSettings', 'NurseController@accountSettings');
	Route::post('nurse/accountSettings/update', 'NurseController@accountSettingsUpdate');
	Route::get('nurse/passwordSettings', 'NurseController@passwordSettings');
	Route::post('nurse/passwordSettings/update', 'NurseController@passwordSettingsUpdate');
	Route::post('nurse/services/process', 'NurseController@updateServices');

	Route::get('nurse/schedule/deleteSchedule/{id}', 'NurseController@deleteScheduleProcess');
});	
<div class="section" style="padding:0 1.5em;">
	<div class="row">
		<p class="caption">General informations about the nurse.</p>
		<div class="divider" id="divhr"></div>
		
		<!-- ERRORS -->
		<div class="card-panel white-text hide" id="div_errors">
			<ul class="fa-ul error_list"></ul>
		</div>
		
		<div class="card-panel">
			<form id="accountForm">
				<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
				<h5>Name</h5>
				<div class="row"> <!-- start row -->
					<!-- first name -->
					<div class="input-field col s12 l4">
						<input name="first_name" type="text" value="{{$user->first_name}}" required />
						<label for="first_name">First</label>
					</div>
					<div class="input-field col s12 l4">
						<input name="middle_name" type="text" value="{{$user->middle_name}}" required />
						<label for="middle_name">Middle</label>
					</div>
					<div class="input-field col s12 l4">
						<input name="last_name" type="text" value="{{$user->last_name}}" required />
						<label for="last_name">Last</label>
					</div>
				</div> <!-- end row s -->

				<h5> Status</h5>
				<div class="row"> <!-- start row -->
					<div class="input-field col s12 l4">
						<select name="gender">
							<option value="" selected>Choose your Gender</option>
							<option value="Male" {{ ($user->gender == 'Male') ? 'selected' : '' }}>Male</option>
							<option value="Female" {{ ($user->gender == 'Female') ? 'selected' : '' }}>Female</option>
						</select>
						<label>Select Gender</label>
					</div>
					<div class="input-field col s12 l4">
						<select name="civil_status">
							<option value="" selected>What's your Status</option>
							<option value="Single" {{ ($user->civil_status == 'Single') ? 'selected' : '' }}>Single</option>
							<option value="Married" {{ ($user->civil_status == 'Married') ? 'selected' : '' }}>Married</option>
							<option value="Divorced" {{ ($user->civil_status == 'Divorced') ? 'selected' : '' }}>Divorced</option>
							<option value="Separated" {{ ($user->civil_status == 'Separated') ? 'selected' : '' }}>Separated</option>
							<option value="Widowed" {{ ($user->civil_status == 'Widowed') ? 'selected' : '' }}>Widowed</option>
						</select>
						<label>Civil Status</label>
					</div>
					<div class="input-field col s12 l4">
						<input name="birthdate" type="date" value="{{$user->birthdate}}" required />
						<label for="birthdate" class="active">Birthdate</label>
					</div>
				</div> <!-- end row -->

				<h5> Complete Address</h5>
				<div class="row"> <!-- start row -->
					<div class="input-field col s12 l3">
						<input name="street" type="text" value="{{$user->street}}" required />
						<label for="street">House number, Street</label>
					</div>
					<div class="input-field col s12 l3">
						<input name="barangay" type="text" value="{{$user->barangay}}" required />
						<label for="barangay">Subdivision / Barangay</label>
					</div>
					<div class="input-field col s12 l3">
						<input name="town" type="text" value="{{$user->town}}" required />
						<label for="town">City / Town</label>
					</div>
					<div class="input-field col s12 l3">
						<select name="province" required >
							<option selected disabled>Select your province</option>
							@foreach($provinces as $province)
							<option value="{{ $province->province }}" {{ ($user->province == $province->province) ? 'selected' : '' }}>{{ $province->province }}</option>
							@endforeach
						</select>
						<label>Province</label>
					</div>
				</div> <!-- end row -->

				<h5> Educational Attainment </h5>
				<div class="row"> <!-- start row -->
					<div class="input-field col s12 l6">
						<input name="degree" type="text" value="{{$user->degree}}" required />
						<label for="degree">College Degree</label>
					</div>
					<div class="input-field col s12 l3">
						<input name="year_graduated" type="text" value="{{$user->year_graduated}}" required />
						<label for="year_graduated">Year Graduated</label>
					</div>
					<div class="input-field col s12 l3">
						<input name="school" type="text" value="{{$user->school}}" required />
						<label for="school">School</label>
					</div>
				</div> <!-- end row -->

				<h5> License No. </h5>
				<div class="row"> <!-- start row -->
					<div class="input-field col s12 l12">
						<input name="license" type="text" value="{{$user->license}}" required />
						<label for="license">PRC License # or <span class="red-text">TBD if not yet available</span></label>
					</div>
				</div> <!-- end row -->

				<h5> Contact Information</h5>
				<div class="row"> <!-- start row -->
					<div class="input-field col s12 l4">
						<input type="text" value="{{$user->email}}" disabled>
						<label for="street">Email Address (can't edit for now)</label>
					</div>
					<div class="input-field col s12 l4">
						<input name="cellphone" type="text" value="{{$user->cellphone}}" required />
						<label for="barangay">Cellphone Number</label>
					</div>
					<div class="input-field col s12 l4">
						<input name="landline" type="text" value="{{$user->landline}}" />
						<label for="town">Landline Number</label>
					</div>
				</div> <!-- end row -->
				
				<br>
				<div class="row"> <!-- start row -->
					<div class="input-field col s12 l12">
						<button type="submit" id="accountSubmit" class="btn btn-large waves-effect waves-cyan cyan">
							<i class="material-icons right">cloud</i>Save Changes
						</button>
					</div>
				</div> <!-- end row -->

			</form>
			
		</div>
	</div>
</div>
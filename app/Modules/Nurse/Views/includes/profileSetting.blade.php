<div class="section" style="padding:0 1.5em;">
	<div class="row">
		<p class="caption">Additional broad information about the nurse.</p>
		<div class="divider" id="divhr"></div>
		
		<!-- ERRORS -->
		<div class="card-panel white-text hide" id="div_errors">
			<ul class="error_list"></ul>
		</div>
		
		<div class="card-panel">
			<form id="profileForm">
				<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
				<h5>About Me</h5>
				<div class="row"> <!-- start row -->
					<div class="input-field col s12 l12">
						<textarea id="about_me" name="about" class="materialize-textarea">{{$user->about}}</textarea>
						<label for="about_me">About yourself</label>
					</div>
				</div>

				<h5>Background</h5>
				<div class="row"> <!-- start row -->
					<div class="input-field col s12 l12">
					<textarea id="background" name="background" class="materialize-textarea">{{$user->background}}</textarea>
					<label for="background">Background experience</label>
					</div>
				</div>

				<div class="row"> <!-- start row s -->
					<div class="input-field col s12 l12">
						<button type="submit" id="profileSubmit" class="btn btn-large wave-effects wave-cyan cyan">
							<i class="material-icons right">cloud</i>Save Changes
						</button>
					</div>
				</div>
			</form>
	
		</div>
		
	</div>
</div>
<div class="section" style="padding-top:1em;">
	<p class="caption">Dashboard Overview</p>
	<div class="divider" id="divhr"></div>
	<div class="row">
		<div class="col s12 m12 l8">
			<div class="row">
				<div class="col s12 m12 l12">
					<ul class="collection with-header">
						<li class="collection-header">
							<h5 class="light-blue-text" style="font-size: 1.5em;"><i class="mdi-notification-sms-failed left"></i> Notifications <span class="right">2</span></h5>
							<h6 class="light-blue-text">From your Patients <a href="#!" class="right">View all</a></h6>
							<!-- <button class="btn-floating btn-large right yellow darken-2" style="top: -18px;"><i class="mdi-content-add"></i></button> -->
						</li>

						
						<li class="collection-item padding-0" style="overflow-y: auto;height: 250px">
							<ul class="collection no-border" style="margin: 0px;">

								<!-- loop here -->
								<li class="collection-item padding-5 no-border">
									<div class="row">
										<div class="col s8 m8 l10">
											<p class="black-text"><b>MEAL ASSIST</b></p>
											<p class="grey-text text-darken-2"><strong>Vinyl sustainable everyday carry direct trade, crucifix cold-pressed selfies.</strong></p>
										</div>
										<div class="col s4 m4 l2" style="padding: 20px 0px 0px 0px ">
											<small class="grey-text">Just now</small>
										</div>
									</div>
								</li>

								<li class="collection-item padding-5 no-border">
									<div class="row">
										<div class="col s8 m8 l10">
											<p class="black-text"><b>BATH ASSIST</b></p>
											<p class="grey-text text-darken-2"><strong>Vinyl sustainable everyday carry direct trade, crucifix cold-pressed selfies.</strong></p>
										</div>
										<div class="col s4 m4 l2" style="padding: 20px 0px 0px 0px ">
											<small class="grey-text">1 min ago</small>
										</div>
									</div>
								</li>
								<!-- end loop here -->
							</ul>
						</li>
					</ul>
				</div>
			</div>
		
			<div class="row">
				<div class="col s12 m12 l6">
					<ul class="collection with-header">
						<li class="collection-header light-blue padding-5">
							<h5 class="white-text" style="font-size: 1.3em;"><i class="mdi-communication-message left"></i> Messages <span class="right">3</span></h5>
							<h6 class="white-text" style="font-size: 1em;">Current Conversations <a href="#!" class="right white-text">View all</a></h6>
							<!-- <button class="btn-floating btn-large right yellow darken-2" style="top: -18px;"><i class="mdi-content-add"></i></button> -->
						</li>

						
						<li class="collection-item padding-0" id="email-list"  style="overflow-y: auto;height: 250px">
							<ul class="collection no-border" style="margin: 0px;">

								<!-- loop here -->
								<li class="collection-item padding-5 no-border">
									<div class="row">
										<div class="col s2 m2 l2">
											<div class="img-list" style="background-image: url('{{ asset('assets/images/avatars/img_parent.png') }}')">
												<span class="status circle green"></span>
											</div>
										</div>
										<div class="col s10 m10 l10" style="line-height: 5px;">
											<p class="black-text"><b>Juana Dela Cruz</b></p>
											<p class="grey-text"><small>Just now</small></p>
										</div>
									</div>
								</li>

								<li class="collection-item padding-5 no-border">
									<div class="row">
										<div class="col s2 m2 l2">
											<div class="img-list" style="background-image: url('{{ asset('assets/images/avatars/img_parent.png') }}')">
												<span class="status circle red"></span>
											</div>
										</div>
										<div class="col s10 m10 l10" style="line-height: 5px;">
											<p class="black-text"><b>JD Sarmiento</b></p>
											<p class="grey-text"><small>2 hours ago</small></p>
										</div>
									</div>
								</li>

								<li class="collection-item padding-5 no-border">
									<div class="row">
										<div class="col s2 m2 l2">
											<div class="img-list" style="background-image: url('{{ asset('assets/images/avatars/img_parent.png') }}')">
												<span class="status circle green"></span>
											</div>
										</div>
										<div class="col s10 m10 l10" style="line-height: 5px;">
											<p class="black-text"><b>Louie Villena</b></p>
											<p class="grey-text"><small>2 days ago</small></p>
										</div>
									</div>
								</li>
								<!-- end loop here -->
							</ul>
						</li>
					</ul>
				</div>

				<div class="col s12 m12 l6">
					<ul class="collection with-header">
						<li class="collection-header light-blue padding-5">
							<h5 class="white-text" style="font-size: 1.3em;"><i class="mdi-action-event left"></i> Appointments <span class="right">2</span></h5>
							<h6 class="white-text" style="font-size: 1em;">Current Appointments <a href="{{url('nurse/appointment')}}" class="right white-text">View all</a></h6>
							<!-- <button class="btn-floating btn-large right yellow darken-2" style="top: -18px;"><i class="mdi-content-add"></i></button> -->
						</li>

						
						<li class="collection-item padding-0" style="overflow-y: auto;height: 250px">
							<ul class="collection no-border" style="margin: 0px;">
								<!-- loop here -->
								@if($appointments->count() == 0)
								<li class="collection-item padding-5 no-border">
									<div class="row">
										<div class="col s12 m12 l12">
											<p>No appointments yet</p>
										</div>
									</div>
								</li>
								@else
								@foreach($appointments as $app)
								<li class="collection-item padding-5 no-border">
									<div class="row">
										<div class="col s2 m2 l2">
											<div class="img-list" style="background-image: url('{{ asset('assets/images/avatars/img_parent.png') }}')">
												
											</div>
										</div>

										<div class="col s8 m8 l6" style="line-height: 5px;">
											<p class="black-text"><b>{{ $app->patient->first_name.' '.$app->patient->last_name }}</b></p>
											<p class="grey-text"><small>{{ date_format(date_create($app->date_start), 'F d, Y / h:i A') }}</small></p>
										</div>

										<div class="col s2 m2 l4 center" id="app{{$app->id}}" style="padding: 20px 0px 0px 0px ">
											@if(strtotime($app->date_end) <= strtotime(date('Y-m-d H:i:s')))
											<span class="task-cat grey" style="padding: 6px;">Time past</span>
											@elseif($app->approve == 1)
											<span class="task-cat green">Reserved</span>
											@elseif($app->patient_id == 0)
											<span class="task-cat light-blue">Available</span>
											@else
											<a style="cursor:pointer;" onclick="appointmentAction('{{$app->id}}', '{{$app->patient_id}}', 'approve')"><i class="mdi-navigation-check circle green tiny darken-2 white-text" style="padding: 5px"></i></a>
											<a style="cursor:pointer;" onclick="appointmentAction('{{$app->id}}', '{{$app->patient_id}}', 'decline')"><i class="mdi-navigation-close circle red tiny darken-2 white-text" style="padding: 5px"></i></a>
											@endif
										</div>
									</div>
								</li>
								@endforeach
								@endif
								<!-- end loop here -->
							</ul>
						</li>
					</ul>
				</div>

			</div>

		</div>

		<div class="col s12 m12 l4">
			<ul class="collection with-header">
				<li class="collection-header padding-5">
					<h5 class="light-blue-text" style="font-size: 1.3em;"><i class="mdi-action-account-box left"></i> Patients </h5>
					<h6 class="light-blue-text" style="font-size: 1em;">Your Contact <a href="#!" class="right white-text">View all</a></h6>
					<!-- <button class="btn-floating btn-large right yellow darken-2" style="top: -18px;"><i class="mdi-content-add"></i></button> -->
				</li>

				
				<li class="collection-item padding-0" id="email-list"  style="overflow-y: auto;height: 615px">
					<ul class="collection no-border" style="margin: 0px;">

						<!-- loop here -->
						<li class="collection-item padding-5 no-border">
							<div class="row">
								<div class="col s2 m2 l2">
									<div class="img-list" style="background-image: url('{{ asset('assets/images/avatars/img_parent.png') }}')">
										
									</div>
								</div>
								<div class="col s10 m10 l10" style="line-height: 5px;">
									<p class="black-text"><b>Ira Mendoza</b></p>
									<p class="grey-text"><small><i class="mdi-maps-place light-blue-text tiny"></i></small> Makati City</p>
								</div>
							</div>
						</li>

						<li class="collection-item padding-5 no-border">
							<div class="row">
								<div class="col s2 m2 l2">
									<div class="img-list" style="background-image: url('{{ asset('assets/images/avatars/img_parent.png') }}')">
										
									</div>
								</div>
								<div class="col s10 m10 l10" style="line-height: 5px;">
									<p class="black-text"><b>Ira Mendoza</b></p>
									<p class="grey-text"><small><i class="mdi-maps-place light-blue-text tiny"></i></small> Makati City</p>
								</div>
							</div>
						</li>
						<!-- end loop here -->
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="section">
	<p class="caption">Nurse schedules of upcomming appointments.</p>
	<div class="divider" id="divhr"></div>
	<div class="row">
		<div class="col s12 m6 l6">
			<ul class="collection with-header">
				<li class="collection-header light-blue padding-5">
					<h5 class="white-text" style="font-size: 1.3em;"><i class="mdi-action-event left"></i> Schedules <span class="right">2</span></h5>
					<h6 class="white-text" style="font-size: 1em;">Current Schedules <a href="#!" class="right white-text">View all</a></h6>
				</li>

				<li class="collection-item padding-0" id="email-list" style="overflow-y: auto;height: 300px">
					<ul class="collection no-border" style="margin: 0px;">
						<!-- loop here -->
						@if($schedule->count() != 0)
						@foreach($schedule as $sched)
						<li class="collection-item padding-5 no-border" id="schedNum{{$sched->id}}">
							<div class="row">
								<div class="col s5 m5 l5" style="line-height: 5px;">
									<p class="black-text"><b>{{date_format(date_create($sched->date_start), 'F d, Y')}}</b></p>
									<p class="grey-text">{{date_format(date_create($sched->date_start), 'h:i A')}}</p>
									
								</div>
								<div class="col s5 m5 l5 center" style="padding:20px 0 0 0;">
									@if(strtotime($sched->date_end) <= strtotime(date('Y-m-d H:i:s')))
									<span class="task-cat grey" style="padding: 6px;">Time past</span>
									@elseif($sched->reserve == 1 && $sched->approve == 1)
									<span class="task-cat blue-grey" style="padding: 6px;">reserved</span>
									@elseif($sched->reserve == 1)
									<span class="task-cat light-blue" style="padding: 6px;">pending</span>
									@else
									<span class="task-cat green" style="padding: 6px;">available</span>
									@endif
								</div>
								<div class="col s2 m2 l2 center" style="padding: 20px 0px 0px 0px "> 
									<a href="{{url('nurse/schedule/editSchedule')}}/{{$sched->id}}"><i class="mdi-editor-mode-edit circle green tiny darken-2 white-text" style="padding: 5px"></i></a>
									<a onClick="deleteSchedule({{$sched->id}});"><i class="mdi-navigation-close circle red tiny darken-2 white-text" style="padding: 5px"></i></a>
								</div>
							</div>
						</li>
						@endforeach
						@else
						<li class="collection-item padding-5 no-border">
							<h3 align="center">No Schedule Found</h3>
						</li>
						@endif
						<!-- end loop here -->
					</ul>
				</li>
			</ul>
		</div>
		
		<div class="col s12 m6 l6">
			<div class="card-panel">
				<div id="schedule"></div>
			</div>
		</div>
	</div>
</div>

<!-- FAB -->
<div class="fixed-action-btn">
	<a href="{{ url('nurse/schedule/addSchedule') }}" class="btn-floating btn-large red waves-effect tooltipped" data-position="left" data-tooltip="Create new schedule">
		<i class="mdi-content-add"></i>
	</a>
</div>
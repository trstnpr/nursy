<div class="section">
	<p class="caption">Nurse list of appointments.</p>
	<div class="divider" id="divhr"></div>
	<div class="row">
		<div class="col s12 m12 l12">
			<ul class="collection with-header">
				<li class="collection-header light-blue padding-5">
					<h5 class="white-text" style="font-size: 1.3em;"><i class="mdi-action-event left"></i> Appointments <span class="right">{{count($appointments['present']) + count($appointments['past'])}}</span></h5>
					<div class="row">
						<div class="col s12 l6">
							<ul class="tabs transparent white-text" style="text-align:left;">
								<li class="tab col s3"><a href="#test1" class="active white-text">Current Appointments</a></li>
								<li class="tab col s3"><a href="#test2" class="white-text">Past Appointments</a></li>
							</ul>
						</div>
					</div>
					<!-- <button class="btn-floating btn-large right yellow darken-2" style="top: -18px;"><i class="mdi-content-add"></i></button> -->
				</li>

				
				<li class="collection-item padding-0" style="overflow-y: auto;min-height: 350px;max-height:500px;">
					<div id="test1">
						<ul class="collection no-border" style="margin: 0px;">
							<!-- loop here -->
							@if(count($appointments['present']) == 0)
								<li class="collection-item padding-5 no-border">
									<div class="row">
										<div class="col s12 m12 l12 center">
											<p>NO AVAILABLE APPOINTMENTS</p>
										</div>
									</div>
								</li>
							@else
								@foreach($appointments['present'] as $app)
								<li class="collection-item padding-5 no-border">
									<div class="row">
										<div class="col s2 m2 l1">
											<div class="img-list" style="background-image: url('{{ asset('assets/images/avatars/img_parent.png') }}')">
												
											</div>
										</div>
										<div class="col s8 m8 l10" style="line-height: 5px;">
											<p class="black-text"><b>{{$app->patient['first_name'].' '.$app->patient['last_name']}}</b></p>
											<p class="grey-text">{{date_format(date_create($app->date_start), 'F d, Y | h:i A')}}</p>
										</div>

										<div class="col s2 m2 l1 center" id="app{{$app->id}}" style="padding: 20px 0px 0px 0px ">
											@if($app->approve == 1)
											<span class="task-cat green">Reserved</span>
											@elseif($app->patient_id == 0)
											<span class="task-cat light-blue">Available</span>
											@else
											<a style="cursor:pointer;" onclick="appointmentAction('{{$app->id}}', '{{$app->patient_id}}', 'approve')"><i class="mdi-navigation-check circle green tiny darken-2 white-text" style="padding: 5px"></i></a>
											<a style="cursor:pointer;" onclick="appointmentAction('{{$app->id}}', '{{$app->patient_id}}', 'decline')"><i class="mdi-navigation-close circle red tiny darken-2 white-text" style="padding: 5px"></i></a>
											@endif
										</div>
									</div>
								</li>
								@endforeach
							@endif
						</ul>
					</div>


					<div id="test2">
						<ul class="collection no-border" style="margin: 0px;">

							<!-- loop here -->
							@if(count($appointments['past']) == 0)
								<li class="collection-item padding-5 no-border">
									<div class="row">
										<div class="col s12 m12 l12 center">
											<p>NO PAST APPOINTMENTS</p>
										</div>
									</div>
								</li>
							@else
								@foreach($appointments['past'] as $app)
								<li class="collection-item padding-5 no-border">
									<div class="row">
										<div class="col s2 m2 l1">
											<div class="img-list" style="background-image: url('{{ asset('assets/images/avatars/img_parent.png') }}')">
												
											</div>
										</div>
										<div class="col s8 m8 l10" style="line-height: 5px;">
											<p class="black-text"><b>{{$app->patient['first_name'].' '.$app->patient['last_name']}}</b></p>
											<p class="grey-text">{{date_format(date_create($app->date_start), 'F d, Y | h:i A')}}</p>
										</div>
									</div>
								</li>
								@endforeach
							@endif
						</ul>
					</div>

				</li>
			</ul>
		</div>
	</div>
</div>
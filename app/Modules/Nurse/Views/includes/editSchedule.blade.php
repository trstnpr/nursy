<div class="section">
	<p class="caption">Nurse schedule update.</p>
	<div class="divider" id="divhr"></div>
	
	<div class="card-panel blue lighten-4">
		<span class="valign-wrapper"><i class="medium left material-icons">info</i> In updating schedule, choose a date, starting time and ending time. Patients can choose timeslots between those hours. Make sure you are available for house calls between these hours.</span>
	</div>
	
	<div class="card-panel">
		<form method="POST" id="editScheduleForm">
			<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
			<input type="hidden" name="sched_id" value="{{$schedule->id}}">
			<div class="row">
			
				<div class="input-field col s12 hide" id="error_div">
					<div class="card-panel red ligthen-2 white-text" id="child_div">
						<strong><i class="mdi-alert-warning"></i> Error Message</strong>
						<ul class="error_list"></ul>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<i class="material-icons prefix">date_range</i>
					<input type="date" name="date" class="datepicker" value="{{ date_format(date_create($schedule->date_start), 'd F, Y') }}">
					<label for="date">Date</label>
				</div>
			</div>

			<div class="row">
				<div class="input-field col s12 m6">
					<i class="material-icons prefix">av_timer</i>
					<input type="text" name="time_start" id="time-picker" value="{{ date_format(date_create($schedule->date_start), 'h:i A') }}">
					<label for="Time">Time Start</label>
				</div>
				<div class="input-field col s12 m6">
					<i class="material-icons prefix">av_timer</i>
					<input type="text" name="time_end" id="time-end-picker" value="{{ date_format(date_create($schedule->date_end), 'h:i A') }}">
					<label for="Time">Time End</label>
				</div>
			</div>

			<div class="row">
				<div class="input-field col s12">
					 <button type="submit" id="editScheduleSubmit" class="btn btn-large blue-grey waves-effect"><i class="material-icons left">update</i>Update</button>
				</div>
			</div>
		</form>
	</div>
</div>
<p class="caption">Nurse services and duties.</p>
<div class="divider" id="divhr"></div>
<div class="section">
	<!-- ERRORS -->
	<div class="card-panel row white-text hide" id="div_errors">
		<ul class="error_list"></ul>
	</div>
	
	<div class="card-panel row grey lighten-4 z-depth-1">
		<div class="col s12 m12 l12">
			<h5><b>{{ $user->first_name }} {{ $user->last_name }}</b> <small>{{ ($user->nurse_type != null) ? $user->nurse_type : 'Nurse' }} <i class="mdi-action-verified-user tiny blue-text"></i></small></h5>
			<h6><b><i class="fa fa-check green-text"></i> Services Offered</b></h6>
			<p>{{ ($user->services != null) ? $user->services : 'No Services' }}</p>
		</div>
	</div>
	
	<div class="card-panel row">
		<div class="col s12 m12 l12">
			<h5>Nurse Services</h5>
		</div>
		<form id="serviceForm">	
			<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
			<div class="input-field col s12 m12 l12">
				<select id="nurseType" name="nurseType">
					<option disabled selected>Choose your option</option>
					@foreach($nurse_type as $type)
					<option value="{{$type->nurse}}">{{$type->nurse}}</option>
					@endforeach
				</select>
				<label><big>I am a</big></label>
			</div>
			<div class="input-field col s12 m12 l12 hide" id="svcOffer">
				<h6 id="checkCaption"></h6>
				<div class="row">
					<div class="input-field col s12 m12">
						<input type="checkbox" class="filled-in" id="checkAll" />
						<label for="checkAll">Check All</label>
					</div>
					@foreach($nurse_services as $services)
					<div class="input-field col s12 m3 field{{$services->id}}">
						<input type="checkbox" class="filled-in cbx cbx{{$services->id}}" name="services[]" id="service{{$services->id}}" value="{{ $services->service }}"/>
						<label for="service{{$services->id}}">{{ $services->service }}</label>
					</div>
					@endforeach
				</div>
				<br/>
			</div>
			
			<div class="input-field col s12 m12 l12">
				<button type="submit" class="btn waves-effect waves-light cyan btn-large" id="serviceSubmit">
					<i class="material-icons right">cloud</i>Save Changes
				</button>
			</div>
		</form>
	</div>
</div>
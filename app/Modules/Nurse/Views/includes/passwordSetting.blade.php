<div class="section" style="padding:0 1.5em;">
	<div class="row">
		<p class="caption">Nurse password management.</p>
		<div class="divider" id="divhr"></div>
		
		<!-- ERRORS -->
		<div class="card-panel white-text hide" id="div_errors">
			<ul class="error_list"></ul>
		</div>
		
		<div class="card-panel">
			<form id="passwordForm">
				<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
				<h5>Your Current Password</h5>
				<div class="row"> <!-- start row -->
					<div class="input-field col s12 l12">
						<input name="current_password" type="password"></input>
						<label for="current_password">Current Password</label>
					</div>
				</div>
				
				<h5>Your New Password</h5>
				<div class="row"> <!-- start row -->
					<div class="input-field col s12 l6">
						<input name="password" type="password"></input>
						<label for="password">New Password</label>
					</div>
					<div class="input-field col s12 l6">
						<input name="password_confirmation" type="password"></input>
						<label for="password_confirmation">Confirm New Password</label>
					</div>
				</div>
				
				<div class="row"> <!-- start row s -->
					<div class="col s12 l12">
						<button type="submit" id="passwordSubmit" class="btn btn-large wave-effects wave-cyan cyan">
							<i class="material-icons right">cloud</i>Save Changes
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
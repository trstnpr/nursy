@extends('layout-user.nurse')

@section('content')
<!--start container s-->
<div class="container" id="user">
	<!-- CONTENT -->
	@include('Nurse::includes.dashBoard')
</div>
@stop

@section('customScript')
	<script type="text/javascript">
		function appointmentAction(id, pid, type){
			var data = {
				_token : '{{Session::token()}}',
				id     : id,
				pid    : pid,
				type   : type
			}
			$.ajax({
				type: "POST",
				url: "{{ url('appointmentAction') }}",
				data: data,
				cache: false,
				success: function(data){
					var msg = JSON.parse(data);
					if(msg.dialog == 'Approved your schedule'){
						$('#app'+id).html('<span class="task-cat green">Reserved</span>');
					} else if(msg.dialog == 'Declined your schedule'){
						$('#app'+id).html('<span class="task-cat red">Declined</span>');
					}
				}
			});
		}
	</script>
@stop
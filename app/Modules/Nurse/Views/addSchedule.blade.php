@extends('layout-user.nurse')

@section('content')
<!--breadcrumbs start-->
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
	<div class="row">
		<div class="col s12 m12 l12">
			<h5 class="breadcrumbs-title">Create Schedule</h5>
			<ol class="breadcrumb">
				<li><a href="{{ url('nurse/userDashBoard')}}">Dashboard</a>
				</li>
				<li><a href="{{ url('nurse/schedule')}}">Schedule</a>
				</li>
				<li class="active">Create Schedule</li>
			</ol>
		</div>
	</div>
</div>
<!--breadcrumbs end-->

<!--start container s-->
<div class="container" id="user"> 
	<!-- CONTENT -->
	@include('Nurse::includes.addSchedule')
</div>
@stop

@section('style')
<style type="text/css">
	#user.container {
	  padding: 0 0.5rem;
	  margin: 0 auto;
	  max-width: 100% !important;
	  width: 100%;
	}

</style>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/datetime/lolliclock.css') }}">
@stop

@section('customScript')
	<script type="text/javascript" src="{{ asset('assets/js/datetime/lolliclock.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(e) {
			$('#time-picker').lolliclock({
				autoclose:true
			});
			$('#time-end-picker').lolliclock({
				autoclose:true
			});


			$("#addScheduleForm").on('submit',(function(e) {
				e.preventDefault();
				$.ajax({
					url: "{{url('nurse/schedule/addSchedule/process')}}", // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					cache: false,             // To unable request pages to be cached
					processData:false,        // To send DOMDocument or non processed data file it is set to false
					beforeSend: function(){ $("#addScheduleSubmit").html('Creating...');},
					error: function(data){ // A function to be called if request failed
						if(data.readyState == 4){
							errors = JSON.parse(data.responseText);
							$('.error_list').empty();
							$.each(errors,function(key,value){
								$.each(value,function(k,val){
								$('.error_list').append("<li><strong class='valign-wrapper'><i class='mdi-navigation-close'></i> "+val+"</strong></li>");
								});
							});
							$("#child_div").removeClass('green').addClass('red');
							$('#error_div').removeClass('hide');
						}
						$("#addScheduleSubmit").html('Create');
					},
					success: function(data){ // A function to be called if request succeeds
						var msg = JSON.parse(data);
						if(msg.result == 'success'){
							$("#addScheduleSubmit").html('Create');
							$("#child_div").removeClass('red').addClass('green').html("<strong><i class='mdi-navigation-check'></i> Schedule Created! </strong>");
							$("#error_div").removeClass('hide');
							window.location.href='{{ url('/nurse/schedule') }}';
						} else{
							$("#addScheduleSubmit").html('Create');
							$("#child_div").removeClass('green').addClass('red').html("<strong class='valign-wrapper'><i class='mdi-navigation-close'></i> "+msg.dialog+" </strong>");
							$("#error_div").removeClass('hide');
						}
					}
				});
			}));
		});

	</script>
@stop
@extends('layout-user.nurse')

@section('content')
<!--breadcrumbs start-->
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
	<div class="row">
		<div class="col s12 m12 l12">
			<h5 class="breadcrumbs-title">DHTMLX</h5>
			<ol class="breadcrumb">
				<li><a href="{{ url('admin/')}}">Dashboard</a>
				</li>
				<li class="active">DHTMLX</li>
			</ol>
		</div>
	</div>
</div>
<!--breadcrumbs end-->

<!--start container-->
<div class="container" id="user">
	 <div class="section">
		<!-- CONTENT s -->
		@include('Nurse::includes.dhtmlx')
    </div>

</div>
@stop

@section('customScript')
	<script src="{{ asset('assets/dhtmlxscheduler/codebase/dhtmlxscheduler.js') }}" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="{{ asset('assets/dhtmlxscheduler/codebase/dhtmlxscheduler.css') }}" type="text/css" media="screen" title="no title" charset="utf-8">

	<script type="text/javascript" charset="utf-8">
		$(document).ready(function(e) {
			$("#scheduler_here").dhx_scheduler({
				xml_date:"%Y-%m-%d %H:%i",
				date:new Date(2016,5,19),
				mode:"month"
			});

			scheduler.load("data/events.xml");
		});
	</script>
@stop
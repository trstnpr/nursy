@extends('layout-user.nurse')

@section('content')
<!--breadcrumbs start-->
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
	<div class="row">
		<div class="col s12 m12 l12">
			<h5 class="breadcrumbs-title">Schedule</h5>
			<ol class="breadcrumb">
				<li><a href="{{ url('nurse/userDashBoard')}}">Dashboard</a>
				</li>
				<li class="active">Schedule</li>
			</ol>
		</div>
	</div>
</div>
<!--breadcrumbs end-->

<!--start container s-->
<div class="container" id="user">
	<!-- CONTENT -->
	@include('Nurse::includes.scheduleSetting')

    <!-- MODAL FOR SCHEDULE PER DATE -->
    <!-- Modal Structure -->
	<div id="modal-schedule" class="modal">
	    <div class="modal-content">
	    	<h4>Modal Header</h4>
	    	<p>A bunch of text</p>
	    </div>
	</div>
</div>
@stop

@section('style')
<style rel="stylesheet" type="text/css" href="{{ asset('assets/css/zabuto/zabuto_calendar.css') }}"></style>
@stop

@section('customScript')
	<script type="text/javascript" src="{{ asset('assets/js/zabuto/zabuto_calendar.js') }}"></script>
	<script type="application/javascript">
		$(document).ready(function() {
				
			 $("#schedule").zabuto_calendar({
				today: true,
				nav_icon: {
					prev: '<i class="mdi-navigation-chevron-left"></i>',
					next: '<i class="mdi-navigation-chevron-right"></i>'
				},
				ajax: {
					url: "{{ url('getNurseSchedule') }}/all/{{ $user->id }}",
					modal: false
				},
				action: function() {
					showEventModal(this.id);
				}
			 });
		});

		function showEventModal(id){
			var date = $("#" + id).data("date");
			var hasEvent = $("#" + id).data("hasEvent");
			var data = {};
			if(hasEvent == true){
				$('#modal-schedule .modal-content').css({'background-color': 'rgba(0,0,0,0.5)'}).html('<div style="padding:100px;color:#fff;"><center><i class="fa fa-spinner fa-pulse fa-4x"></i><br> <h4>Loading</h4></center></div>');
			    $('#modal-schedule').openModal();
				$.ajax({
					type: "GET",
					url: "{{ url('getNurseSchedule') }}/"+date+"/{{ $user->id }}",
					data: data,
					processData:false,
		            contentType:false,
		            async: true,
		            cache:false,
					success: function(data){
						var msg = JSON.parse(data);
						$('#modal-schedule .modal-content').css({'background-color': '#fff'}).html(msg);
					}
				});
		  	}
		}

		function deleteSchedule(id){
			alertify.confirm("DELETE SCHEDULE", "Are you sure you want to delete this schedule?",
			function(){
				$.ajax({
					type: "GET",
					url: "{{ url('nurse/schedule/deleteSchedule') }}/"+id,
					processData:false,
		            contentType:false,
		            async: true,
		            cache:false,
					success: function(data){
						var msg = JSON.parse(data);
						if(msg.result == 'success'){
							alertify.success(msg.dialog);
							setInterval(function(){ window.location.reload(); }, 1500);
						} else {
							alertify.error(msg.dialog);
						}
					}
				});
			},
			function(){
				alertify.error('Cancel');
			});
		}
	</script>
@stop
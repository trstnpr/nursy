@extends('layout-user.nurse')

@section('content')
<!--breadcrumbs start-->
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
	<div class="row">
		<div class="col s12 m12 l12">
			<h5 class="breadcrumbs-title">Appointments</h5>
			<ol class="breadcrumb">
				<li><a href="{{ url('nurse/userDashBoard')}}">Dashboard</a>
				</li>
				<li class="active">Appointments</li>
			</ol>
		</div>
	</div>
</div>
<!--breadcrumbs end-->

<!--start container s-->
<div class="container" id="user">
	<!-- CONTENT -->
	@include('Nurse::includes.appointment')
</div>
@stop

@section('customScript')
	<script type="application/javascript">
		function appointmentAction(id, pid, type){
			var data = {
				_token : '{{Session::token()}}',
				id     : id,
				pid    : pid,
				type   : type
			}
			$.ajax({
				type: "POST",
				url: "{{ url('appointmentAction') }}",
				data: data,
				cache: false,
				success: function(data){
					var msg = JSON.parse(data);
					if(msg.dialog == 'Approved your schedule'){
						$('#app'+id).html('<span class="task-cat green">Reserved</span>');
					} else if(msg.dialog == 'Declined your schedule'){
						$('#app'+id).html('<span class="task-cat red">Declined</span>');
					}
				}
			});
		}
	</script>
@stop
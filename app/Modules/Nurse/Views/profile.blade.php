@extends('layout-user.nurse')

@section('content')
<!--breadcrumbs start-->
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
	<div class="row">
		<div class="col s12 m12 l12">
			<h5 class="breadcrumbs-title">Profile Management</h5>
			<ol class="breadcrumb">
				<li><a href="{{ url('nurse/userDashBoard')}}">Dashboard</a>
				</li>
				<li><a href="{{ url('nurse/userDashBoard')}}">Settings</a>
				</li>
				<li class="active">Profile Settings</li>
			</ol>
		</div>
	</div>
</div>
<!--breadcrumbs end-->

<!--start container-->
<div class="container" id="user">
	<!-- CONTENT s -->
	@include('Nurse::includes.profileSetting')
</div>
@stop

@section('customScript')
	<script type="text/javascript">
		$(document).ready(function(e) {
			$("#profileForm").on('submit',(function(e) {
				e.preventDefault();
				$.ajax({
					url: "{{url('nurse/profileSettings/update')}}", // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					cache: false,             // To unable request pages to be cached
					processData:false,        // To send DOMDocument or non processed data file it is set to false
					beforeSend: function(){ $("#profileSubmit").html('Saving...');},
					error: function(data){ // A function to be called if request failed
						if(data.readyState == 4){
							errors = JSON.parse(data.responseText);
							$('.error_list').empty();
							$.each(errors,function(key,value){
								$.each(value,function(k,val){
								$('.error_list').append("<li><strong class='valign-wrapper'><i class='mdi-navigation-close'></i> "+val+"</strong></li>");
								});
							});
							$("#div_errors").removeClass('hide green').addClass('red');
							$("html, body").animate({ scrollTop: 0 }, "slow");
							alertify.error('Oops! Something went wrong...');
						}
						$("#profileSubmit").html('<i class="material-icons right">cloud</i>Save Changes');
					},
					success: function(data){ // A function to be called if request succeeds
						var msg = JSON.parse(data);
						if(msg.result == 'success'){
							$("html, body").animate({ scrollTop: 0 }, "slow");
							$("#div_errors").removeClass('red').addClass('hide');
							$("#profileSubmit").html('<i class="material-icons right">cloud</i>Save Changes');
							alertify.success('Successfully Saved!');
						}
					}
				});
			}));
		});
	</script>
@stop
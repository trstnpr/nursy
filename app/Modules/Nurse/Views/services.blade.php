@extends('layout-user.nurse')

@section('content')
<!--breadcrumbs start-->
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
	<div class="row">
		<div class="col s12 m12 l12">
			<h5 class="breadcrumbs-title">Services</h5>
			<ol class="breadcrumb">
				<li><a href="{{ url('nurse/userDashBoard')}}">Dashboard</a>
				</li>
				<li class="active">Services</li>
			</ol>
		</div>
	</div>
</div>
<!--breadcrumbs end-->

<!--start container-->
<div class="container" id="user">
	 <div class="section">
		<!-- CONTENT s -->
		@include('Nurse::includes.services')
    </div>

</div>
@stop

@section('customScript')
	<script type="text/javascript">
		$(document).ready(function(e) {
			
			$('#dismissAlert').click(function() {
				$('#div_errors').addClass('hide');
			});

			$('#nurseType').change(function() {
				if($('#nurseType').val()) {
					$('#svcOffer').removeClass('hide');
					$('#checkCaption').html('Services offered by a '+$('#nurseType').val());
					 
						// cbx div.input-field
					var aide_mid = $('.field1, .field2, .field3, .field4, .field5, .field6, .field7, .field8, .field9, .field10, .field11'),
						r_nurse = $('.field12, .field13, .field14, .field15'),
						r_nurse_spec = $('.field16, .field17'),
						// cbx input.filled-in
						cbx_na_midw = $('.cbx1, .cbx2, .cbx3, .cbx4, .cbx5, .cbx6, .cbx7, .cbx8, .cbx9, .cbx10, .cbx11'),
						cbx_rn = $('.cbx12, .cbx13, .cbx14, .cbx15'),
						cbx_rns = $('.cbx16, .cbx17');
					
					// Thru select value
					if($('#nurseType').val() == 'Nursing Aide / Caregiver' || $('#nurseType').val() == 'Midwife / Caregiver') {
						$('#checkAll, .cbx').removeAttr('checked');
						if(r_nurse.hasClass('hide') === false) {
							r_nurse.addClass('hide');
						}
						if(r_nurse_spec.hasClass('hide') === false) {
							r_nurse_spec.addClass('hide');
						}
						aide_mid.removeClass('hide');
						$('#checkAll').change(function () {
							cbx_na_midw.prop('checked', $(this).prop('checked'));
						});
						
					} else if($('#nurseType').val() == 'Registered Nurse') {
						$('#checkAll, .cbx').removeAttr('checked');
						if(r_nurse_spec.hasClass('hide') === false) {
							r_nurse_spec.addClass('hide');
						} else {
							r_nurse.removeClass('hide');
							aide_mid.removeClass('hide');
						}
						$('#checkAll').change(function () {
							cbx_na_midw.prop('checked', $(this).prop('checked'));
							cbx_rn.prop('checked', $(this).prop('checked'));
						});
					} else if($('#nurseType').val() == 'Registered Nurse (Specialized)') {
						$('#checkAll, .cbx').removeAttr('checked');
						if(aide_mid.hasClass('hide')) {
							aide_mid.removeClass('hide');
							r_nurse.removeClass('hide');
							r_nurse_spec.removeClass('hide');
						} else {
							r_nurse.removeClass('hide');
							r_nurse_spec.removeClass('hide');
						}
						$('#checkAll').change(function () {
							cbx_na_midw.prop('checked', $(this).prop('checked'));
							cbx_rn.prop('checked', $(this).prop('checked'));
							cbx_rns.prop('checked', $(this).prop('checked'));
						});
					}
					
					// Thru database table
					
				}
			});
			
			
			$("#serviceForm").on('submit',(function(e) {
				e.preventDefault();
				$.ajax({
					url: "{{url('nurse/services/process')}}", // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					cache: false,             // To unable request pages to be cached
					processData:false,        // To send DOMDocument or non processed data file it is set to false
					beforeSend: function(){ $("#serviceSubmit").html('Saving...');},
					error: function(data){ // A function to be called if request failed
						if(data.readyState == 4){
							errors = JSON.parse(data.responseText);
							$('.error_list').empty();
							$.each(errors,function(key,value){
								$.each(value,function(k,val){
								$('.error_list').append("<li><strong class='valign-wrapper'><i class='mdi-navigation-close'></i> "+val+"</strong></li>");
								});
							});
							$("#div_errors").removeClass('hide green').addClass('red');
							$("html, body").animate({ scrollTop: 0 }, "slow");
							alertify.error('Oops! Something went wrong...');
						}
						$("#serviceSubmit").html('<i class="material-icons right">cloud</i>Save Changes');
					},
					success: function(data){ // A function to be called if request succeeds
						var msg = JSON.parse(data);
						if(msg.result == 'success'){
							$("html, body").animate({ scrollTop: 0 }, "slow");
							$("#div_errors").removeClass('red').addClass('hide');
							$("#serviceSubmit").html('<i class="material-icons right">cloud</i>Save Changes');
							alertify.success('Successfully Saved!');
						}
					}
				});
			}));
			
		});
	</script>
@stop
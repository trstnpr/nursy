@extends('layout-user.patient')

@section('content')
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
	<div class="row">
		<div class="col s12 m12 l12">
			<h5 class="breadcrumbs-title">Nurse Profile</h5>
			<ol class="breadcrumb">
				<li><a href="{{ url('patient/userDashBoard')}}">Dashboard</a></li>
				<li><a href="{{ url('patient/findNurse')}}">Find Nurse</a></li>
				<li class="active">{{$nurse->first_name}} {{$nurse->last_name}}</li>
			</ol>
		</div>
	</div>
</div>

<!--start container s-->
<div class="container" id="user">
	<!-- CONTENT -->
	@include('Patient::includes.profile')
</div>
@stop

@section('style')
<style type="text/css">
	#user .nurse-img {
		height: 100px; width: 100px;
		background-size: cover;
		border: 2px solid #eee;
		margin: 10px;
	}
</style>
@stop

@section('customScript')
	<script type="application/javascript">
	  $(document).ready(function() {
	    $('#available_schedule').zabuto_calendar();
	  });
	</script>
@stop
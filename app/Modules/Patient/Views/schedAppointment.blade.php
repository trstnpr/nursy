@extends('layout-user.patient')

@section('content')
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
	<div class="row">
		<div class="col s10 m10 l10">
			<h5 class="breadcrumbs-title">Nurse Schedule</h5>
			<ol class="breadcrumb">
				<li><a href="{{ url('patient/userDashBoard')}}">Dashboard</a></li>
				<li><a href="{{ url('patient/findNurse')}}">Find Nurse</a></li>
				<li class="active">{{$nurse->first_name}} {{$nurse->last_name}}</li>
			</ol>
		</div>
	</div>
</div>

<!--start container s-->
<div class="container" id="user">
	<!-- CONTENT -->
	@include('Patient::includes.schedAppointment-content')
</div>

	<!-- MODAL FOR SCHEDULE PER DATE -->
    <!-- Modal Structure -->
	<div id="modal-schedule" class="modal">
	    <div class="modal-content">
	    	<h4>Modal Header</h4>
	    	<p>A bunch of text</p>
	    </div>
	</div>

	<!-- MODAL FOR SUCCESS RESERVATION -->
    <!-- Modal Structure -->
	<div id="success-reserve" class="modal">
	    <div class="modal-content">
	    	<h4>Modal Header</h4>
	    	<p>A bunch of text</p>
	    </div>
	</div>
@stop

@section('customScript')
	<script type="text/javascript">
		$(document).ready(function() {
			$('#schedule_calendar').zabuto_calendar({
				today: true,
				nav_icon: {
					prev: '<i class="mdi-navigation-chevron-left"></i>',
					next: '<i class="mdi-navigation-chevron-right"></i>'
				},
				ajax: {
					url: "{{ url('getNurseSchedule') }}/all/{{ $nurse->id }}",
					modal: false
				},
				action: function() {
					showEventModal(this.id);
				}
			});
		});

		function showEventModal(id){
			var date = $("#" + id).data("date");
			var hasEvent = $("#" + id).data("hasEvent");
			var data = {};
			if(hasEvent == true){
				$('#modal-schedule .modal-content').css({'background-color': 'rgba(0,0,0,0.5)'}).html('<div style="padding:100px;color:#fff;"><center><i class="fa fa-spinner fa-pulse fa-4x"></i><br> <h4>Loading</h4></center></div>');
			    $('#modal-schedule').openModal();
				$.ajax({
					type: "GET",
					url: "{{ url('getNurseSchedule') }}/"+date+"/{{ $nurse->id }}",
					data: data,
					processData:false,
		            contentType:false,
		            async: true,
		            cache:false,
					success: function(data){
						var msg = JSON.parse(data);
						$('#modal-schedule .modal-content').css({'background-color': '#fff'}).html(msg);
					}
				});
		  	}
		}

		function reserveDoctorSchedule(id, drID, type){
			var data = {
				_token	: '{{ Session::token() }}',
				id		: id,
				dr		: drID,
				type	: type
			}
			$.ajax({
				type: "POST",
				url: "{{ url('/reserveNurseSchedule') }}",
				data: data,
				cache: false,
				beforeSend: function(){ $("#"+id).html('<span class="task-cat pink">Processing...</span>');},
				success: function(data){
					var msg = JSON.parse(data);
					if(msg.dialog == 'Reserved your schedule'){
						$("#"+id).html('<span class="secondary-content task-cat green">Reserved</span>');
						$('#modal-schedule').closeModal();
						$('#success-reserve').html('<div class="modal-content">'+
																		'<h4 class="modal-title">Successfully Reserved</h4>'+
															        	'<p style="color:#555;font-size:1.2em;">You have successfully created an appointment with Dr. '+msg.doctor_name+' on'+
															        	'<strong> '+msg.date+' </strong> . '+
															        	'You will get an email and notification if your appointment has been confirmed by doctor.</p>'+
															      '</div>'+
															      '<div class="modal-footer">'+
																        '<a style="cursor:pointer;" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>'+
																  '</div>');
						$('#success-reserve').openModal();

						/*$('#success-reserve').on('hidden.bs.modal', function () {
							window.location.reload();
						})*/
					} else if(msg.dialog == 'Declined your schedule'){
						$("#"+id).html('cancel');
						alert("Reservation Cancelled!");
						window.location.reload();
					} else{
						$("#"+id).html('Error');
					}
				}
			});
		}
	</script>
@stop
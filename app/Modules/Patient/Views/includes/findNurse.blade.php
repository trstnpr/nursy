<div class="section">
	<p class="caption">Nurse List</p>
	<div class="divider" id="divhr"></div>
	
	<div class="card-panel z-depth-1">
		<div class="row">
			<form>
				<div class="input-field col m4 s12">
					<i class="mdi-action-search prefix"></i>
					<input type="text" id="key" name="key" value="{{app('request')->input('key')}}" />
					<label for="key">Search for keywords</label>
				</div>
				<div class="input-field col m3 s12">
					<select name="type" id="type">
						<option value="">Select type</option>
						@foreach($nurse_type as $type)
						<option value="{{ $type->nurse }}" {{ ($type->nurse == app('request')->input('type')) ? 'selected' : '' }}>{{ $type->nurse }}</option>
						@endforeach
					</select>
					<label>Filter by Type</label>
				</div>
				<div class="input-field col m3 s12">
					<select name="location" id="location">
						<option value="">Select location</option>
						@foreach($provinces as $province)
						<option value="{{ $province->province }}" {{ ($province->province == app('request')->input('location')) ? 'selected' : '' }}>{{ $province->province }}</option>
						@endforeach
					</select>
					<label>Filter by Location</label>
				</div>
				<div class="input-field col m2 s12">
					<button type="button" id="searchSubmit" class="btn col s12 cyan waves-effect waves-light">Search</button>
				</div>
			</form>
		</div>
	</div>

	<div class="row">
	<!-- looping here -->
	@if($nurseList->count() == 0)
		<div class="col m12">
			<div class="card-panel">
				<h3 class="center">Nothing to Display.</h3>
			</div>
		</div>
	@else
		@foreach($nurseList as $nurse)
			<div class="col s12 m4 l3">
				<div class="card" style="padding-bottom: 0px !important;">
					<div class="card-image" align="center" id="nurse_card">
						@if(is_null($nurse->image))
							<div class="nurse-list" style="background-image: url('{{ asset('assets/images/avatars/img_parent.png')}}');"></div>
						@else
							<div class="nurse-list" style="background-image: url('https://s3-ap-southeast-1.amazonaws.com/nursy/{{ $nurse->image }}');"></div>
						@endif
					</div>

					<div class="card-content center">
						<h5><strong>{{$nurse->first_name}} {{$nurse->last_name}}</strong></h5>
						<h6>{{ ($nurse->nurse_type != null) ? $nurse->nurse_type : 'Nurse' }}</h6>
						<a href="{{url('patient/nurseProfile')}}/{{$nurse->id}}">See Profile</a>
					</div>
					<button onclick="location.href='{{url('patient/schedAppointment')}}/{{$nurse->id}}'" class="btn col m12 white-text blue-grey btn-large waves-effect"> Schedule an Appointment</button> 
				</div>
			</div>
		@endforeach
		<div class="col m12">
			<hr/>
		</div>
	@endif
	<!-- end of looping here -->
	</div>
</div>
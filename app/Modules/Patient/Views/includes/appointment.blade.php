<div class="section">
	<p class="caption">Appointments list</p>
	<div class="divider" id="divhr"></div>
	<div class="row">
		<div class="col s12 m12 l12">
			<ul class="collection with-header">
		        <li class="collection-header light-blue padding-5">
		        	<h5 class="white-text" style="font-size: 1.3em;"><i class="mdi-action-event left"></i> Appointments</h5>
		        	<div class="row">
		        		<div class="col s12 l6">
		        			<ul class="tabs transparent white-text" style="text-align:left;">
						        <li class="tab col s3"><a href="#test1" class="active white-text">Current</a></li>
						        <li class="tab col s3"><a href="#test2" class="white-text">Previous</a></li>
						    </ul>
		        		</div>
		        	</div>
		        </li>
		        
		        <li class="collection-item padding-0" style="overflow-y: auto;min-height: 350px;max-height:500px;">
		        	<div id="test1">
			        	<ul class="collection no-border" style="margin: 0px;">
		    				<!-- loop here -->
		    				@if(count($appointments['present']) == 0)
		    				<li class="collection-item padding-5 lighten-1">
		        				<div class="row">
					        		<div class="col s12 m12 l12 center">
					        			<p>No current appointments</p>
					        		</div>
					        	</div>
				       		</li>
		    				@else
		    				@foreach($appointments['present'] as $app)
		    				<li class="collection-item padding-5 no-border">
		        				<div class="row">
					        		<div class="col s2 m2 l1">
					        			<div class="img-list" style="background-image: url('{{ asset('assets/images/avatars/img_parent.png') }}')">
					        				
					        			</div>
					        		</div>
					        		<div class="col s8 m8 l10" style="line-height: 5px;">
					        			<p class="black-text"><b>{{$app->nurse->first_name.' '.$app->nurse->last_name}}</b></p>
					        			<p class="grey-text">{{date_format(date_create($app->date_start), 'F d, Y h:i A')}}</p>
					        		</div>

					        		<div class="col s2 m2 l1 center" style="padding: 20px 0px 0px 0px ">
					        			@if(strtotime($app->date_end) <= strtotime(date('Y-m-d H:i:s')))
										<span class="task-cat grey" style="padding: 6px;">Time past</span>
										@elseif($app->reserve == 1 && $app->approve == 1)
										<span class="task-cat green" style="padding: 6px;">approved</span>
										@elseif($app->reserve == 1)
										<span class="task-cat light-blue" style="padding: 6px;">requested</span>
										@endif
					        		</div>
					        	</div>
				       		</li>
		    				@endforeach
		    				@endif
			        	</ul>
		        	</div>


		        	<div id="test2">
			        	<ul class="collection no-border" style="margin: 0px;">

		    				<!-- loop here -->
		    				@if(count($appointments['past']) == 0)
		    				<li class="collection-item padding-5 lighten-1">
		        				<div class="row">
					        		<div class="col s12 m12 l12 center">
					        			<p>No past appointments</p>
					        		</div>
					        	</div>
				       		</li>
		    				@else
								@foreach($appointments['past'] as $app)
								<li class="collection-item padding-5 no-border">
									<div class="row">
										<div class="col s2 m2 l1">
											<div class="img-list" style="background-image: url('{{ asset('assets/images/avatars/img_parent.png') }}')">
												
											</div>
										</div>
										<div class="col s8 m8 l10" style="line-height: 5px;">
											<p class="black-text"><b>{{$app->nurse->first_name.' '.$app->nurse->last_name}}</b></p>
											<p class="grey-text">{{date_format(date_create($app->date_start), 'F d, Y h:i A')}}</p>
										</div>

										<div class="col s2 m2 l1 center" style="padding: 20px 0px 0px 0px ">
											@if(strtotime($app->date_end) <= strtotime(date('Y-m-d H:i:s')))
											<span class="task-cat grey" style="padding: 6px;">Time past</span>
											@elseif($app->reserve == 1 && $app->approve == 1)
											<span class="task-cat green" style="padding: 6px;">approved</span>
											@elseif($app->reserve == 1)
											<span class="task-cat light-blue" style="padding: 6px;">requested</span>
											@endif
										</div>
									</div>
								</li>
								@endforeach
		    				@endif

			        	</ul>
		        	</div>

		        </li>
		    </ul>
		</div>
	</div>
</div>

<!-- FAB -->
<div class="fixed-action-btn">
	<a href="{{url('patient/findNurse')}}" class="btn-floating btn-large yellow darken-2 tooltipped" data-position="left" data-tooltip="Find Nurse">
		<i class="mdi-action-search"></i>
	</a>
</div>
<div class="section">
	<p class="caption">Nurse {{$nurse->first_name}} {{$nurse->last_name}}'s schedule.</p>
	<div class="divider"></div>
	<div class="row">
		<div class="col s12 m12 l8">
			<ul class="collection with-header">
		        <li class="collection-header light-blue white-text"><h5><i class="fa fa-thumb-tack left"></i> Schedule of {{$nurse->first_name}} {{$nurse->last_name}}</h5></li>
		        <li class="collection-item" id="calendar-container">
		        	<div id="schedule_calendar"></div>
		        </li>
		    </ul>
		</div>

		<div class="col s12 m12 l4">
			<ul class="collection with-header">
		        <li class="collection-header light-blue">
		        	<h5 class="white-text"><i class="fa fa-history left"></i> Appointment Logs <span class="right">2</span></h5>
		        </li>
		        <li class="collection-item padding-0" id="email-list"  style="overflow-y: auto;height: 200px">
		        	<ul class="collection no-border" style="margin: 0px;">

		        		<!-- loop here -->
		        		<li class="collection-item padding-5 no-border">
		        			<div class="row">
				        		<div class="col s2 m2 l2">
				        			<div class="img-list" style="background-image: url('{{ asset('assets/images/avatars/img_parent.png') }}')">
				        				
				        			</div>
				        		</div>
				        		<div class="col s8 m8 l6" style="line-height: 5px;">
				        			<p class="black-text"><b>Ira Mendoza</b></p>
				        			<p class="grey-text">Apr 15, 2015 / 2:00 PM</p>
				        		</div>

				        		<div class="col s2 m2 l4 center" style="padding: 20px 0px 0px 0px "> 
				        			<span class="task-cat grey" style="padding: 6px;">requested</span>
				        		</div>
				        	</div>
		        		</li>

		        		<li class="collection-item padding-5 no-border">
		        			<div class="row">
				        		<div class="col s2 m2 l2">
				        			<div class="img-list" style="background-image: url('{{ asset('assets/images/avatars/img_parent.png') }}')">
				        				
				        			</div>
				        		</div>
				        		<div class="col s8 m8 l6" style="line-height: 5px;">
				        			<p class="black-text"><b>Ira Mendoza</b></p>
				        			<p class="grey-text">Apr 15, 2015 / 2:00 PM</p>
				        		</div>

				        		<div class="col s2 m2 l4 center" style="padding: 20px 0px 0px 0px "> 
				        			<i class="mdi-navigation-check circle green tiny darken-2 white-text" style="padding: 5px"></i>
				        		</div>
				        	</div>
		        		</li>
		        		<!-- end loop here -->
		        	</ul>
		        </li>
		    </ul>
		</div>
	</div>
</div>

<!-- FAB -->
<div class="fixed-action-btn">
	<a href="{{url('patient/nurseProfile')}}/{{$nurse->id}}" class="btn-floating btn-large yellow darken-2 tooltipped" data-position="left" data-tooltip="See {{$nurse->first_name}}'s Profile">
		<i class="mdi-action-perm-identity"></i>
	</a>
</div>
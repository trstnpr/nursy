<div class="section">
	<p class="caption">Nurse {{$nurse->first_name}} {{$nurse->last_name}}'s profile.</p>
	<div class="divider"></div>
	<div class="row">
		<div class="col s12 m12 l6">
			<div class="card" id="profile-card">
				<div class="card-image waves-effect waves-block waves-light">
					<img class="activator" src="{{ url('assets/images/background/cross.jpg') }}" alt="Profile" />
				</div>
				<div class="card-content">
					<img src="https://s3-ap-southeast-1.amazonaws.com/nursy/{{ $nurse->image }}" alt="Photo" class="circle responsive-img card-profile-image" />
					<a href="#feedbackModal" class="btn-floating activator btn-move-up waves-effect waves-light darken-2 cyan right tooltipped modal-trigger" data-position="left" data-tooltip="Give a feedback">
						<i class="mdi-editor-mode-edit"></i>
					</a>
					<span class="card-title grey-text text-darken-4">
						{{$nurse->first_name}} {{$nurse->last_name}}
						<i class="mdi-action-verified-user blue-text tooltipped" data-tooltip="Verified"></i>
					</span>
					<p>
						<i class="mdi-action-perm-identity cyan-text text-darken-2"></i>
						{{ ($nurse->nurse_type != null) ? $nurse->nurse_type : 'Nurse' }}
					</p>
					<p>
						<i class="mdi-action-picture-in-picture cyan-text text-darken-2"></i>
						PRC License No. {{ ($nurse->license != null) ? $nurse->license : 'TBD' }}
					</p>
					<p>
						<i class="mdi-maps-place cyan-text text-darken-2"></i>
						@if(!is_null($nurse->town) || !is_null($nurse->province))
							{{$nurse->town}}, {{$nurse->province}}
						@else
							No address given
						@endif
					</p>
				</div>
			</div>
			
			<div class="card-panel">
				<button class="btn btn-flat blue-grey darken-3 white-text">Services</button>
				<div class="divider"></div>
				<h6><b><i class="fa fa-check green-text"></i> Services Offered</b></h6>
				<p>{{ ($nurse->services != null) ? $nurse->services : 'No Service' }}</p>
				
				<button class="btn btn-flat blue-grey darken-3 white-text">More</button>
				<div class="divider"></div>
				<h5>About</h5>
				<p>
				@if(is_null($nurse->about))
					None
				@else
					{{$nurse->about}}
				@endif
				</p>
				<h5>Background</h5>
				<p>
				@if(is_null($nurse->background))
					None
				@else
					{{$nurse->background}}
				@endif
				</p>
			</div>
		</div>

		<div class="col s12 m12 l6">
			<div class="card-panel teal">
				<h5 class="white-text"><i class="mdi-editor-insert-comment"></i> Feedbacks <span class="badge white-text"><big>4</big></span></h5>
			</div>
			<div class="card-panel teal" style="overflow-y:auto;max-height:500px;">
				@for($z=0; $z < 10; $z++)
				<div class="card-panel grey lighten-5 z-depth-1">
					<div class="row valign-wrapper">
						<div class="col m2 hide-on-small-only">
							<img src="https://s3-ap-southeast-1.amazonaws.com/nursy/{{ $nurse->image }}" alt="" class="circle responsive-img" />
						</div>
						<div class="col m10">
							<p><b>Billy Danovan</b> <span class="right">1/1/2016</span></p>
							<span class="black-text">
								<b>Awesome</b>. This is a square image. Add the "circle" class to it to make it appear circular.
							</span>
						</div>
					</div>
				</div>
				@endFor
			</div>
		</div>
	</div>
</div>

<!-- Modal Review -->
<div id="feedbackModal" class="modal">
	<div class="modal-content cyan lighten-5">
		<h4>Give a Feedback</h4>
		<div class="input-field col s12">
			<i class="mdi-action-assessment prefix"></i>
			<input id="feed_word" name="feed_word" type="text">
			<label for="feed_word">Give me a one liner...</label>
		</div>
		<div class="input-field col s12">
			<i class="mdi-editor-mode-edit prefix"></i>
			<textarea id="feed_content" class="materialize-textarea" name="feed_content"></textarea>
			<label for="feed_content" class="">Say something...</label>
		</div>
	</div>
	<div class="modal-footer cyan lighten-3">
		<button class="modal-action modal-close waves-effect waves-green btn-flat" type="submit" name="action">Submit</button>
		<button class="modal-action modal-close waves-effect waves-green btn-flat" name="action">Cancel</button>
	</div>
</div>

<!-- FAB -->
<div class="fixed-action-btn">
	<a href="{{url('patient/schedAppointment')}}/{{$nurse->id}}" class="btn-floating btn-large yellow darken-2 tooltipped" data-position="left" data-tooltip="See {{$nurse->first_name}}'s Schedule">
		<i class="mdi-action-schedule"></i>
	</a>
</div>


@extends('layout-user.patient')

@section('content')
<!--breadcrumbs start-->
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
	<div class="row">
		<div class="col s12 m12 l12">
			<h5 class="breadcrumbs-title">Appointments</h5>
			<ol class="breadcrumb">
				<li><a href="{{ url('patient/userDashBoard')}}">Dashboard</a>
				</li>
				<li class="active">Appointments</li>
			</ol>
		</div>
	</div>
</div>
<!--breadcrumbs end-->

<!--start container s-->
<div class="container" id="user">
	<!-- CONTENT -->
	@include('Patient::includes.appointment')
</div>
@stop

@section('style')
	<style type="text/css">
		.collection.with-header .collection-header {
		    border-bottom: 0px solid #e0e0e0;
		}
	</style>
@stop

@section('customScript')
	<script type="text/javascript">
		

	</script>
@stop
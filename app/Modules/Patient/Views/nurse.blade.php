@extends('layout-user.patient')

@section('content')
<!--breadcrumbs start-->
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
	<div class="row">
		<div class="col s12 m12 l12">
			<h5 class="breadcrumbs-title">Find Nurse</h5>
			<ol class="breadcrumb">
				<li><a href="{{ url('patient/userDashBoard')}}">Dashboard</a></li>
				<li class="active">Find Nurse</li>
			</ol>
		</div>
	</div>
</div>
<!--breadcrumbs end-->

<!--start container s-->
	<!-- CONTENT -->
	<div class="container" id="user">
		@include('Patient::includes.findNurse')
	</div>
@stop

@section('style')
	<style type="text/css">
		#user .nurse-list {
			height: 150px; width: 150px;
			background-size: cover;
			border: 2px solid #eee;
			border-radius: 50%;
		}
	</style>
@stop

@section('customScript')
	<script type="text/javascript">
		$(document).ready(function(){
			$('#searchSubmit').click(function(){
				var key = $('#key').val();
				var type = $('#type').val();
				var location = $('#location').val();

				if(key != "" && type != "" && location != ""){
                    window.location.href = "{{url('patient/findNurse')}}/?key="+key+"&type="+type+"&location="+location;
                } else if(key == "" && type != "" && location != ""){
                    window.location.href = "{{url('patient/findNurse')}}/?type="+type+"&location="+location;
                } else if(key != "" && type == "" && location != ""){
                    window.location.href = "{{url('patient/findNurse')}}/?key="+key+"&location="+location;
                } else if(key != "" && type != "" && location == ""){
                    window.location.href = "{{url('patient/findNurse')}}/?key="+key+"&type="+type;
                } else if(key != "" && type == "" && location == ""){
                    window.location.href = "{{url('patient/findNurse')}}/?key="+key;
                } else if(key == "" && type != "" && location == ""){
                    window.location.href = "{{url('patient/findNurse')}}/?type="+type;
                } else if(key == "" && type == "" && location != ""){
                    window.location.href = "{{url('patient/findNurse')}}/?location="+location;
                } else {
                    window.location.href = "{{url('patient/findNurse')}}";
                }
			});
		});
	</script>
@stop
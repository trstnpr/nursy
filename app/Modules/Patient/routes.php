<?php

Route::group(array('module' => 'Patient', 'middleware' => ['web', 'auth', 'patient_role'], 'namespace' => 'App\Modules\Patient\Controllers'), function() {
	Route::get('patient/userDashBoard', 'PatientController@dashBoard');
	Route::get('patient/findNurse', 'PatientController@findNurse');
	Route::get('patient/appointment', 'PatientController@appointment');
	Route::get('patient/accountSettings', 'PatientController@accountSettings');
	Route::post('patient/accountSettings/update', 'PatientController@accountSettingsUpdate');
	Route::get('patient/passwordSettings', 'PatientController@passwordSettings');
	Route::post('patient/passwordSettings/update', 'PatientController@passwordSettingsUpdate');
	Route::get('patient/schedAppointment/{id}', 'PatientController@schedAppointment');
	Route::get('patient/nurseProfile/{id}', 'PatientController@nurseProfile');
	Route::post('patient/profileSettings/update', 'PatientController@profileSettingsUpdate');
});	
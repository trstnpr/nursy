<?php namespace App\Modules\Patient\Controllers;

use Auth;
use Hash;
//REQUESTS
use App\Http\Requests;
use App\Http\Requests\PatientAccountRequest;
use App\Http\Requests\PasswordRequest;
//MODELS
use App\User;
use App\Models\Province;
use App\Modules\Patient\Models\Patient;
use App\Modules\Nurse\Models\Nurse;
use App\Models\Services;
use App\Models\NurseType;
use App\Modules\Schedule\Models\Schedule;
//CONTROLLERS
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PatientController extends Controller {

	public function __construct()
	{
		$this->data['user'] = Auth::user();
	}

	public function profileSettings()
	{
		return view("Patient::profile", $this->data);
	}

	public function profileSettingsUpdate(Request $req)
	{
		$input = $req->all();
		unset($input['_token']);
		foreach ($input as $key => $value) {
			if(empty($value)){
				$input[$key] = null;
			}
		}

		$result = Patient::where('id', '=', Auth::user()->id)->update($input);
		if($result){
			return json_encode(array('result'=>'success'));
		} else {
			return json_encode(array('result'=>'error', 'dialog'=>'There\'s an error updating your account'));
		}
	}

	public function accountSettings()
	{
		$this->data['provinces'] = Province::all();
		return view("Patient::account", $this->data);
	}

	public function accountSettingsUpdate(PatientAccountRequest $req)
	{
		$input = $req->all();
		unset($input['_token']);
		foreach ($input as $key => $value) {
			if(empty($value)){
				$input[$key] = null;
			}
		}

		$result = Patient::where('id', '=', Auth::user()->id)->update($input);
		if($result){
			return json_encode(array('result'=>'success'));
		} else {
			return json_encode(array('result'=>'error', 'dialog'=>'There\'s an error updating your account'));
		}
	}

	public function passwordSettings()
	{
		return view("Patient::password", $this->data);
	}

	public function passwordSettingsUpdate(PasswordRequest $req)
	{
		if(Hash::check($req->current_password, Auth::user()->password)){
			
			$input['password'] = Hash::make($req->password);

			Patient::where(array('id' => Auth::user()->id))->update($input);

			return json_encode(array('result'=>'success'));

		} else{
			return json_encode(array('result'=>'error', 'dialog'=>'Old Password does not match!'));
		}
	}

	public function findNurse(Request $req)
	{
		if(!empty($req->input('key')) || !empty($req->input('type')) || !empty($req->input('location'))){
			// function search
			$key = $req->input('key');
			$type = $req->input('type');
			$location = $req->input('location');
			
			if(empty($req->input('key')) && !empty($req->input('type')) && !empty($req->input('location'))){
				//if key is empty
				$this->data['nurseList'] = Nurse::where('role_id', '=', 2)->where('publish', '=', 1)
					->where('nurse_type', 'like', '%'.$req->input('type').'%')
					->where('province', 'like', '%'.$req->input('location').'%')
					->get();

			} else if(!empty($req->input('key')) && empty($req->input('type')) && !empty($req->input('location'))){
				//if type is empty
				$this->data['nurseList'] = Nurse::where('role_id', '=', 2)->where('publish', '=', 1)
					->where('province', 'like', '%'.$req->input('location').'%')
					->where(function($query) use ($key){
						$query->where('first_name', 'like', '%'.$key.'%')
						->orWhere('middle_name', 'like', '%'.$key.'%')
						->orWhere('last_name', 'like', '%'.$key.'%')
						->orWhere('town', 'like', '%'.$key.'%')
						->orWhere('province', 'like', '%'.$key.'%')
						->orWhere('services', 'like', '%'.$key.'%');
					})
					->get();
			} else if(!empty($req->input('key')) && !empty($req->input('type')) && empty($req->input('location'))){
				//if location is empty
				$this->data['nurseList'] = Nurse::where('role_id', '=', 2)->where('publish', '=', 1)
					->where('nurse_type', 'like', '%'.$req->input('type').'%')
					->where(function($query) use ($key){
						$query->where('first_name', 'like', '%'.$key.'%')
						->orWhere('middle_name', 'like', '%'.$key.'%')
						->orWhere('last_name', 'like', '%'.$key.'%')
						->orWhere('town', 'like', '%'.$key.'%')
						->orWhere('province', 'like', '%'.$key.'%')
						->orWhere('services', 'like', '%'.$key.'%');
					})
					->get();
			} else if(!empty($req->input('key')) && empty($req->input('type')) && empty($req->input('location'))){
				//if type and location is empty
				$this->data['nurseList'] = Nurse::where('role_id', '=', 2)->where('publish', '=', 1)
					->where(function($query) use ($key){
						$query->where('first_name', 'like', '%'.$key.'%')
						->orWhere('middle_name', 'like', '%'.$key.'%')
						->orWhere('last_name', 'like', '%'.$key.'%')
						->orWhere('town', 'like', '%'.$key.'%')
						->orWhere('province', 'like', '%'.$key.'%')
						->orWhere('services', 'like', '%'.$key.'%');
					})
					->get();
			} else if(empty($req->input('key')) && !empty($req->input('type')) && empty($req->input('location'))){
				//if key and location is empty
				$this->data['nurseList'] = Nurse::where('role_id', '=', 2)->where('publish', '=', 1)
					->where('nurse_type', 'like', '%'.$req->input('type').'%')
					->get();
			} else if(empty($req->input('key')) && empty($req->input('type')) && !empty($req->input('location'))){
				//if key and type is empty
				$this->data['nurseList'] = Nurse::where('role_id', '=', 2)->where('publish', '=', 1)
					->where('province', 'like', '%'.$req->input('location').'%')
					->get();
			} else {
				$this->data['nurseList'] = Nurse::where('role_id', '=', 2)->where('publish', '=', 1)
					->where('nurse_type', 'like', '%'.$req->input('type').'%')
					->where('province', 'like', '%'.$req->input('location').'%')
					->where(function($query) use ($key){
						$query->where('first_name', 'like', '%'.$key.'%')
						->orWhere('middle_name', 'like', '%'.$key.'%')
						->orWhere('last_name', 'like', '%'.$key.'%')
						->orWhere('town', 'like', '%'.$key.'%')
						->orWhere('province', 'like', '%'.$key.'%')
						->orWhere('services', 'like', '%'.$key.'%');
					})
					->get();
			}
		} else {
			// normal listing
			$this->data['nurseList'] = Nurse::where('role_id', '=', 2)->where('publish', '=', 1)->get();
		}

		$this->data['nurse_services'] = Services::all();
		$this->data['nurse_type'] = NurseType::all();
		$this->data['provinces'] = Province::all();
		return view("Patient::nurse", $this->data);
	}

	public function nurseProfile($id)
	{
		$this->data['nurse'] = Nurse::where('id', '=', $id)->first();
		return view("Patient::nurseProfile", $this->data);
	}

	//DashBoard
	public function dashBoard()
	{
		$this->data['appointments'] = Schedule::where('patient_id', '=', Auth::user()->id)->orderBy('date_start', 'desc')->get();
		foreach ($this->data['appointments'] as $key => $value) {
			$value['nurse'] = Nurse::where('id', '=', $value->nurse_id)->first();
		}
		return view("Patient::dashBoard", $this->data);
	}

	//appointment
	public function appointment()
	{
		$this->data['appointments']['present'] = Schedule::where('patient_id', '=', Auth::user()->id)->where('date_end', '>=', date('Y-m-d H:i:s'))->orderBy('date_start', 'desc')->get();
		$this->data['appointments']['past'] = Schedule::where('patient_id', '=', Auth::user()->id)->where('date_end', '<', date('Y-m-d H:i:s'))->orderBy('date_start', 'desc')->get();
		foreach ($this->data['appointments']['present'] as $key => $value) {
			$value['nurse'] = Patient::where('id', '=', $value->nurse_id)->first();
		}
		foreach ($this->data['appointments']['past'] as $key => $value) {
			$value['nurse'] = Patient::where('id', '=', $value->nurse_id)->first();
		}
		return view("Patient::appointment", $this->data);
	}

	//Schedule an appointment
	public function schedAppointment($id)
	{
		$this->data['nurse'] = Nurse::where('id', '=', $id)->first();
		return view("Patient::schedAppointment", $this->data);
	}

}
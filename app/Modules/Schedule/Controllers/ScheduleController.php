<?php namespace App\Modules\Schedule\Controllers;

use Auth;

use App\User;
use App\Modules\Schedule\Models\Schedule;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ScheduleController extends Controller {

	public function getNurseSchedule($sort, $id)
	{
		if($sort == 'all'){
			$dates = array();
			$date_query = Schedule::where('date_start', '>', '(NOW(), INTERVAL 1 DAY)')->where('nurse_id', '=', $id)->orderBy('date_start', 'asc')->get();
			$i = 0;

			$date_array = array();
			foreach ($date_query as $row) {
				if (strtotime($row['date_start']) < strtotime(date('Y-m-d H:i:s'))){
				
				} else {
					$row['date_start'] = date_format(date_create($row['date_start']), 'Y-m-d');
					if(in_array($row['date_start'], $date_array)){

					} else{
						array_push($date_array, $row['date_start']);
						$date = $row['date_start'];
						$dates[$i] = array(
							'date' => $date,
							'badge' => true
						);
						$i++;
					}
				}
			}

			echo json_encode($dates);
		} else {
			$dates = array();

			$date_start = date_format(date_create($sort), 'Y-m-d H:i:s');
			$date_end = date_format(date_create($sort.' 24:00:00'), 'Y-m-d H:i:s');
			$date_query = Schedule::where('date_start', '>=', $date_start)->where('date_start', '<=', $date_end)->where('nurse_id', '=', $id)->orderBy('date_start', 'asc')->get();
			$content = '<ul class="collection with-header">
	      						<li class="collection-header"><h4><strong class="schedule_date"> Schedule for '.date_format(date_create($sort), 'F d Y').' </strong></h4></li>
			    	  ';
			foreach ($date_query as $row) {
				if (strtotime($row['date_start']) < strtotime(date('Y-m-d H:i:s'))){
					
				} else {
					if(Auth::user()->role_id == 2){
						
						$res = Schedule::where('id', '=', $row['id'])->first();
						if ($res['approve'] == 1) {
							$content .= '<li class="collection-item"><big>'.date_format(date_create($row['date_start']), "F d Y | h:i A").'</big><span class="secondary-content task-cat blue">APPROVED</span></li>';
						} else if ($res['reserve'] == 1) {
							$content .= '<li class="collection-item"><big>'.date_format(date_create($row['date_start']), "F d Y | h:i A").'</big><span class="secondary-content task-cat red">RESERVED</span></li>';
						} else {
							$content .= '<li class="collection-item"><big>'.date_format(date_create($row['date_start']), "F d Y | h:i A").'</big><span class="secondary-content task-cat green">AVAILABLE</span></li>';
						}

					} else{

						$res = Schedule::where('id', '=', $row['id'])->first();
						if ($res['reserve'] == 1 && $res['patient_id'] == Auth::user()->id) {
							$content .= '<li class="collection-item">
						      				<big>'.date_format(date_create($row['date_start']), "F d Y | h:i A").'</big>
						      				<span id="'.$row['id'].'" style="cursor:pointer;" class="secondary-content task-cat red"  onClick="reserveDoctorSchedule('.$row['id'].', '.$id.', \'unreserve\')">cancel</span>
						      				<span class="secondary-content task-cat green">You are already reserved</span>
						      			</li>';
						} else if($res['reserve'] == 1 && $res['patient_id'] != Auth::user()->id){
							$content .= '<li class="collection-item">
						      				<big>'.date_format(date_create($row['date_start']), "F d Y | h:i A").'</big>
						      				<span class="task-cat orange">Someone already reserved</span>
						      			</li>';
						} else {
							$content .= '<li class="collection-item">
						      				<big>'.date_format(date_create($row['date_start']), "F d Y | h:i A").'</big>
						      				<a id="'.$row['id'].'" onClick="reserveDoctorSchedule('.$row['id'].', '.$id.', \'reserve\')" class="secondary-content" style="cursor:pointer;">
						      				<span class="task-cat blue">Reserve this Slot</span>
						      				</a>
						      			</li>';
						}

					}
				}
			}

			$content .= '</ul>';

			echo json_encode($content);
		}
	}

	public function reserveNurseSchedule($req)
	{
		$sched_id = $req->id;
		$nurse_id = $req->dr;
		$type = $req->type;

		$nurse = User::where('id', '=', $nurse_id)->first();
		$schedule = Schedule::where(array('id'=>$sched_id))->first();

		if($type == 'reserve'){
			$input['patient_id'] = Auth::user()->id;
			$input['reserve'] = 1;
			$input['seen'] = 1;
			$input['patient_seen'] = 0;
			Schedule::where(array('id'=>$sched_id, 'nurse_id'=>$nurse_id))->update($input);
			$testimonial = 'Reserved your schedule';
		} else if($type == 'unreserve'){
			$input['patient_id'] = 0;
			$input['reserve'] = 0;
			$input['seen'] = 0;
			$input['patient_seen'] = 0;
			Schedule::where(array('id'=>$sched_id, 'nurse_id'=>$nurse_id))->update($input);
			$testimonial = 'Declined your schedule';
		}

		$data = array(
			'dialog' => $testimonial, 
			'email' => $nurse->email, 
			'name' => Auth::user()->first_name.' '.Auth::user()->last_name,
			'doctor_name' => $nurse->first_name.' '.$nurse->last_name,
			'date' => date_format(date_create($schedule['date_start']), "F j, Y h:i A")
			);

		/*Mail::send('Patient::email-reserve', $data, function($message) use ($data){
			$message->to($data['email'], $data['name'])->subject('Schedule Reservation - Prosheba');
		});*/

		return json_encode($data);
	}

	public function appointmentAction($req)
	{
		$sched_id = $req->id;
		$patient_id = $req->pid;
		$type = $req->type;

		$patient = User::where('id', '=', $patient_id)->first();
		$schedule = Schedule::where(array('id'=>$sched_id))->first();

		if($type == 'approve'){
			$input['approve'] = 1;
			$input['reserve'] = 1;
			$input['seen'] = 0;
			$input['patient_seen'] = 1;
			Schedule::where(array('id'=>$sched_id, 'patient_id'=>$patient_id))->update($input);
			$testimonial = 'Approved your schedule';
		} else if($type == 'decline'){
			$input['patient_id'] = 0;
			$input['reserve'] = 0;
			$input['seen'] = 0;
			$input['patient_seen'] = 1;
			Schedule::where(array('id'=>$sched_id, 'patient_id'=>$patient_id))->update($input);
			$testimonial = 'Declined your schedule';
		}

		$data = array(
			'dialog' => $testimonial, 
			'email' => $patient->email, 
			'name' => Auth::user()->first_name.' '.Auth::user()->last_name,
			'doctor_name' => $patient->first_name.' '.$patient->last_name,
			'date' => date_format(date_create($schedule['date_start']), "F j, Y h:i A")
			);

		/*Mail::send('Patient::email-reserve', $data, function($message) use ($data){
			$message->to($data['email'], $data['name'])->subject('Schedule Reservation - Prosheba');
		});*/

		return json_encode($data);
	}

}

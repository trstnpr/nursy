<?php namespace App\Modules\Schedule\Models;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model {

	/**
     * The database table.
     *
     */
    protected $table = 'schedule';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nurse_id', 'patient_id', 'date_start', 'date_end', 'reserve', 'approve', 'seen', 'patient_seen'
    ];

}

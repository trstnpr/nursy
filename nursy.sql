-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 06, 2016 at 04:49 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nursy`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `created_at`, `updated_at`) VALUES
(1, 'admin', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, '2016-06-24 09:52:27');

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks`
--

CREATE TABLE `feedbacks` (
  `id` bigint(20) NOT NULL,
  `nurse_id` bigint(20) DEFAULT NULL,
  `patient_id` bigint(20) DEFAULT NULL,
  `feedback` text,
  `rating` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `nurse_type`
--

CREATE TABLE `nurse_type` (
  `id` int(11) NOT NULL,
  `nurse` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nurse_type`
--

INSERT INTO `nurse_type` (`id`, `nurse`, `created_at`, `updated_at`) VALUES
(1, 'Nursing Aide / Caregiver', '2016-05-27 00:00:00', '2016-05-27 00:00:00'),
(2, 'Midwife / Caregiver', '2016-05-27 00:00:00', '2016-05-27 00:00:00'),
(3, 'Registered Nurse', '2016-05-27 00:00:00', '2016-05-27 00:00:00'),
(4, 'Registered Nurse (Specialized)', '2016-05-27 00:00:00', '2016-05-27 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `province`
--

CREATE TABLE `province` (
  `id` int(11) NOT NULL,
  `province` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `province`
--

INSERT INTO `province` (`id`, `province`, `created_at`, `updated_at`) VALUES
(1, 'Abra', '2016-05-31 16:46:19', '2016-05-31 16:46:19'),
(2, 'Agusan del Norte', '2016-05-31 16:47:26', '2016-05-31 16:47:26'),
(3, 'Agusan del Sur', '2016-05-31 16:48:10', '2016-05-31 16:48:10'),
(4, 'Aklan', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(5, 'Albay', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(6, 'Antique', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(7, 'Apayao', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(8, 'Basilan', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(9, 'Bataan', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(10, 'Batanes', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(11, 'Batangas', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(12, 'Benguet', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(13, 'Biliran', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(14, 'Bohol', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(15, 'Bukidnon', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(16, 'Bulacan', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(17, 'Cagayan', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(18, 'Camarines Norte', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(19, 'Camarines Sur', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(20, 'Camiguin', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(21, 'Capiz', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(22, 'Catanduanes', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(23, 'Cavite', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(24, 'Cebu', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(25, 'Compostela Valley', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(26, 'Cotabato', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(27, 'Davao del Norte', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(28, 'Davao del Sur', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(29, 'Davao Occidental', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(30, 'Davao Oriental', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(31, 'Dinagat Islands', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(32, 'Eastern Samar', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(33, 'Guimaras', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(34, 'Ifugao', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(35, 'Ilocos Norte', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(36, 'Ilocos Sur', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(37, 'Iloilo', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(38, 'Isabela', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(39, 'Kalinga', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(40, 'La Union', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(41, 'Laguna', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(42, 'Lanao del Norte', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(43, 'Lanao del Sur', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(44, 'Leyte', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(45, 'Maguindanao', '2016-05-31 18:48:45', '2016-05-31 18:48:45'),
(46, 'Marinduque', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(47, 'Masbate', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(48, 'Metro Manila', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(49, 'Misamis Occidental', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(50, 'Misamis Oriental', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(51, 'Mountain Province', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(52, 'Negros Occidental', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(53, 'Negros Oriental', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(54, 'Northern Samar', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(55, 'Nueva Ecija', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(56, 'Nueva Vizcaya', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(57, 'Occidental Mindoro', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(58, 'Oriental Mindoro', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(59, 'Palawan', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(60, 'Pampanga', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(61, 'Pangasinan', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(62, 'Quezon Province', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(63, 'Quirino', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(64, 'Rizal', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(65, 'Romblon', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(66, 'Samar', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(67, 'Sarangani', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(68, 'Siquijor', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(69, 'Sorsogon', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(70, 'South Cotabato', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(71, 'Southern Leyte', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(72, 'Sultan Kudarat', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(73, 'Sulu', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(74, 'Surigao del Norte', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(75, 'Surigao del Sur', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(76, 'Tarlac', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(77, 'Tawi-Tawi', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(78, 'Zambales', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(79, 'Zamboanga del Norte', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(80, 'Zamboanga del Sur', '2016-05-31 18:48:46', '2016-05-31 18:48:46'),
(81, 'Zamboanga Sibugay', '2016-05-31 18:48:46', '2016-05-31 18:48:46');

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE `schedule` (
  `id` bigint(20) NOT NULL,
  `nurse_id` bigint(20) DEFAULT NULL,
  `patient_id` bigint(20) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `reserve` int(11) NOT NULL DEFAULT '0',
  `approve` int(11) NOT NULL DEFAULT '0',
  `seen` int(11) NOT NULL DEFAULT '0',
  `patient_seen` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`id`, `nurse_id`, `patient_id`, `date_start`, `date_end`, `reserve`, `approve`, `seen`, `patient_seen`, `created_at`, `updated_at`) VALUES
(10, 6, 9, '2016-06-30 18:00:00', '2016-06-30 19:00:00', 1, 0, 1, 0, '2016-06-24 07:34:55', '2016-06-24 08:55:48'),
(12, 6, 0, '2016-06-23 18:00:00', '2016-06-23 19:00:00', 0, 0, 0, 0, '2016-06-24 07:34:55', '2016-06-24 08:55:48');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `service` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `service`, `created_at`, `updated_at`) VALUES
(1, 'Bedside care', '2016-05-27 00:00:00', '2016-05-27 00:00:00'),
(2, 'Bed bath / Hygiene / Grooming', '2016-05-27 00:00:00', '2016-05-27 00:00:00'),
(3, 'Turning & range of motion (ROM)', '2016-05-27 00:00:00', '2016-05-27 00:00:00'),
(4, 'Vital signs monitoring', '2016-05-27 00:00:00', '2016-05-27 00:00:00'),
(5, 'Glucose level monitoring', '2016-05-27 00:00:00', '2016-05-27 00:00:00'),
(6, 'Post operation monitoring', '2016-05-27 00:00:00', '2016-05-27 00:00:00'),
(7, 'Wound care, foot care and dressing changes', '2016-05-27 00:00:00', '2016-05-27 00:00:00'),
(8, 'Ostomy / Colonoscopy teaching & management', '2016-05-27 00:00:00', '2016-05-27 00:00:00'),
(9, 'Medication reconsiliation & reports to doctors', '2016-05-27 00:00:00', '2016-05-27 00:00:00'),
(10, 'Medication administration', '2016-05-27 00:00:00', '2016-05-27 00:00:00'),
(11, 'Endorsement', '2016-05-27 00:00:00', '2016-05-27 00:00:00'),
(12, 'Injections', '2016-05-27 00:00:00', '2016-05-27 00:00:00'),
(13, 'Heparin flushes', '2016-05-27 00:00:00', '2016-05-27 00:00:00'),
(14, 'Foley catheter care', '2016-05-27 00:00:00', '2016-05-27 00:00:00'),
(15, 'IV management', '2016-05-27 00:00:00', '2016-05-27 00:00:00'),
(16, 'Cardiac monitoring management', '2016-05-27 00:00:00', '2016-05-27 00:00:00'),
(17, 'Peritoneal dialysis change', '2016-05-27 00:00:00', '2016-05-27 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `civil_status` varchar(255) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `barangay` varchar(255) DEFAULT NULL,
  `town` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `degree` varchar(255) DEFAULT NULL,
  `year_graduated` varchar(255) DEFAULT NULL,
  `school` varchar(255) DEFAULT NULL,
  `license` varchar(255) DEFAULT NULL,
  `about` longtext,
  `background` longtext,
  `cellphone` varchar(255) DEFAULT NULL,
  `landline` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `nurse_type` varchar(100) NOT NULL COMMENT 'for nurse',
  `services` varchar(1000) NOT NULL COMMENT 'for nurse',
  `password` varchar(255) DEFAULT NULL,
  `email_activation_code` varchar(255) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `publish` int(11) NOT NULL DEFAULT '0',
  `role_id` int(11) DEFAULT NULL,
  `password_recover` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `remember_token` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `image`, `first_name`, `middle_name`, `last_name`, `gender`, `civil_status`, `birthdate`, `street`, `barangay`, `town`, `province`, `degree`, `year_graduated`, `school`, `license`, `about`, `background`, `cellphone`, `landline`, `email`, `nurse_type`, `services`, `password`, `email_activation_code`, `active`, `publish`, `role_id`, `password_recover`, `created_at`, `updated_at`, `remember_token`) VALUES
(6, '6/N8iO4f5s.jpg', 'Louie', 'Nurse', 'Villena', NULL, NULL, NULL, NULL, NULL, NULL, 'Pampanga', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'louie.villena05@gmail.com', 'Midwife / Caregiver', 'Bedside care | Bed bath / Hygiene / Grooming | Turning & range of motion (ROM) | Vital signs monitoring | Glucose level monitoring | Post operation monitoring | Wound care, foot care and dressing changes | Ostomy / Colonoscopy teaching & management | Medication reconsiliation & reports to doctors | Medication administration | Endorsement | Injections | Heparin flushes | Foley catheter care | IV management | Cardiac monitoring management | Peritoneal dialysis change', '$2y$10$lSv64JZYGxkIkCbOCNx7NeFUk2zNrQFz7bScrK0foioFoziTYVQPW', 'a967bb6564ff770d583b3acf5116f75c', 1, 1, 2, 0, '2016-04-04 06:39:29', '2016-06-24 08:55:20', 'OqZGzGc2O6DHKorwa63xuL6ewWMrAGofujzCrtZPdCQPCSyFyTOx05yTNn39'),
(7, '7/DZ5yYjsm.jpg', 'Jong', 'Reyes', 'Ortega', 'Male', 'Married', '1970-07-28', '7 Steert', 'Molave Park, Merville', 'Paranaque', 'Metro Manila', 'BS nursing', '1980', 'Fatima College', '90210984', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type ''specimen'' book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially "unchanged". It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', '09174889197', NULL, 'aalorro@gmail.com', 'Nursing Aide / Caregiver', '', '$2y$10$OJE1s0qaXNr1aSKi/vd7SeVSJJvJy4cWX8kkTSGc3FYRiQRsOiGvy', 'c14d4e2cd61f5c8620c8d78585f6c229', 1, 1, 2, 0, '2016-04-07 01:52:31', '2016-05-25 15:09:48', 'NLfZPqqGcxFlrxlziTS3wCfn3GuHm1ZMrVJUcwS8hVZBXyVfdw4KhShPB3qP'),
(8, '8/QAzvwf6q.jpg', 'Armi', 'G', 'Nichols', 'Female', 'Married', '1990-01-01', 'Unit 803 Grand Soho Makati', 'Bel-Air', 'Makati City', 'Metro Manila', NULL, NULL, NULL, NULL, NULL, NULL, '09187888909', NULL, 'armando@alorro.com', '', '', '$2y$10$Gffyz2O23sBIdES/qdJ5COuAOJ4q1mdf1M/Oxj0EHHDIlHmldOGM.', '5d5e4c03ecd4b0d1454606a18c4a2e47', 1, 0, 3, 0, '2016-04-07 05:10:41', '2016-05-25 15:03:15', 'q0VaXbdjv9x38f07BxU1fcVSSSvPr5fCZdS90VdMEqZ0QI5wZA9JNqyEgvRL'),
(9, NULL, 'Sample', '', 'Patient', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'louievillena1205@gmail.com', '', '', '$2y$10$CtbwYN.O4FD3jZSeDoYzo.TZZaxbY7rEyRXSS.ihsdBRiYPbLyYQ6', '9c9c177da5c67189c81618ccab52a05a', 1, 0, 3, 0, '2016-04-08 06:09:05', '2016-06-24 10:36:06', 'JIQ170HSlHHDZcF0Yj0ECta1nMKBEp8lUeZgdnvREfMpHrz16QeXwqpD8QD3'),
(10, '10/IvxsxfWP.jpg', 'Jun', '', 'Rivera', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'aa9922@yahoo.com', '', '', '$2y$10$UByGRakuTDxJpe7GZ1rJ8.b7XyfD3jTw38k3KHZbJ8NGV4kCiy4y.', 'd79857e0fd000b07caee0a460333c5a2', 1, 0, 3, 0, '2016-04-11 05:20:47', '2016-04-11 05:41:58', 'cqTBLEaNgfoKeidykW7cJ183cQoyUyeojuI32VwlTmyjtQFWNz6IQxxlIqcA'),
(11, '11/9bRi5V33.jpg', 'Alexi', 'Aalo', 'Alexis', 'Female', 'Single', NULL, '10', 'Mountainview st', 'Cabantuan', 'Nueva Ecija', 'Bachelor of Nursing Management', '1989', 'Our Lady of Sorrow University', '#9202e1dd', 'The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.', '"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', '09167890987', NULL, 'armando@hybols.com', 'Registered Nurse', '', '$2y$10$O19jEKREvbzUEC/02mHL/uLLUe2lpHTiMXvRSyRk8hOUMBT90PqQG', 'bf9935b5549882a166e5ca8214531170', 1, 1, 2, 0, '2016-04-18 05:45:45', '2016-04-27 08:13:10', 'mLqGmz1ddylQEWh7EvZIeYl3JwqAQ2tausTUECb5Wt3EMaMh5f0MkmoK3LFr');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `role_id` int(11) NOT NULL,
  `role` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`role_id`, `role`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '2016-03-28 11:00:59', '2016-03-28 11:00:59'),
(2, 'Nurse', '2016-03-28 11:01:42', '2016-03-28 11:01:42'),
(3, 'Patient', '2016-03-28 11:01:42', '2016-03-28 11:01:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nurse_type`
--
ALTER TABLE `nurse_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `province`
--
ALTER TABLE `province`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `feedbacks`
--
ALTER TABLE `feedbacks`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nurse_type`
--
ALTER TABLE `nurse_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `province`
--
ALTER TABLE `province`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `schedule`
--
ALTER TABLE `schedule`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `user_role_relationship` FOREIGN KEY (`role_id`) REFERENCES `user_role` (`role_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

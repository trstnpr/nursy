@extends('layouts.app')

@section('content')
<section id="register">
    <div class="wrap">
        <div class="row login-page">
            <div class="col s12 m6 offset-m3 l4 offset-l4">
                <div class="center">
                    <img src="{{ asset('assets/images/logo/nursy-logo-2.png') }}" alt="" class="responsive-img">
                    <p class="center">Join our community now !</p>
                </div>

                <form role="form" method="POST" id="registerForm">
                    {!! csrf_field() !!}
                    <div class="row card-panel blue-grey darken-3">
                        <div class="input-field col s12">
                            <select name="role_id" required="required" id="reg_select">
                                <option disabled>Choose your option</option>
                                <option value="2">Nurse</option>
                                <option value="3">Patient</option>
                            </select>
                            <label>Register As</label>
                        </div>
                    </div>

                    <div class="row card-panel blue-grey darken-4">
                        <div class="input-field col s12 m4">
                            <input id="first_name" name="first_name" type="text" required="required" placeholder="First Name"/>
                            <label for="name">Name</label>
                        </div>
                        <div class="input-field col s12 m4">
                            <input id="middle_name" name="middle_name" type="text" placeholder="Middle Name"/>
                        </div>
                        <div class="input-field col s12 m4">
                            <input id="last_name" name="last_name" type="text" required="required" placeholder="Last Name"/>
                        </div>
                   
                        <div class="input-field col s12">
                            <input id="email" name="email" type="email" class="validate" required="required" placeholder="Email Address"/>
                            <label for="email" data-error="wrong" data-success="right">Email</label>
                        </div>
                    
                        <div class="input-field col s12 m6">
                            <input id="password" name="password" type="password" required="required" placeholder="Password"/>
                            <label for="password">Password</label>
                        </div>
                    
                        <div class="input-field col s12 m6">
                            <input id="password-again" name="password_confirmation" type="password" required="required" placeholder="Confirm password"/>
                        </div>

                        <div class="input-field col s12 hide" id="error_div">
                            <div class="card-panel pink ligthen-2">
                                <ul class="error_list">
                                </ul>
                            </div>
                        </div>

                        <div class="input-field col s12">
                            <div class="g-recaptcha" data-sitekey="6LdcSR0TAAAAAAeebwTnTDKvhEvWJQFkLoHjDC_5"></div>
                        </div>

                        <div class="input-field col s12">
                            <button type="submit" id="register_submit" class="btn btn-large blue-grey darken-2 waves-effect waves-light col s12">Register Now</button>
                        </div>

                        <div class="input-field col s12" id="nurse_text">
                            <div class="card-panel green ligthen-2">
                                <p>PLEASE READ: After creating and verifying your account, your profile will be not be ACTIVE yet. We will ask for a few documentations from you to verify your accreditation as a Registered Nurse. Please be patient while we conduct our due diligence. It will usually take 10-14 business days to conduct a thorough verification of your credentials. You will receive an email once we have finished our verification process. Thank you for your understanding.</p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <p class="center medium-small sign-up">Already have an account? <a href="{{ url('/login') }}" class="light-blue-text">Login</a></p>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('customScript')
<!-- GOOGLE RECAPTCHA -->
<script src='https://www.google.com/recaptcha/api.js'></script>

<script type="text/javascript">
  $(document).ready(function() {
    $("#registerForm").on('submit',(function(e) {
      e.preventDefault();

      var response = grecaptcha.getResponse();

      if(response === ''){
          alert('Check the Captcha first!');
          return false;
      }

      $.ajax({
        url: "{{url('register')}}", // Url to which the request is send
        type: "POST",             // Type of request to be send, called as method
        data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
        contentType: false,       // The content type used when sending data to the server.
        cache: false,             // To unable request pages to be cached
        processData:false,        // To send DOMDocument or non processed data file it is set to false
        beforeSend: function(){ $("#register_submit").html('Processing...');},
        error: function(data){
          //console.log(data);
          if(data.readyState == 4){
              errors = JSON.parse(data.responseText);
              $('.error_list').empty();
              $.each(errors,function(key,value){
                $('.error_list').append('<li>'+value+'</li>');
              });
              $('#error_div').removeClass('hide');
          }
          $("#register_submit").html('Register now');
        },
        success: function(data)   // A function to be called if request succeeds
        {
          //console.log(data);
          var msg = JSON.parse(data);
          if(msg.result == 'success'){
            $("#register_submit").html('Register now');
            alert("Your account was created. Please Check your email to activate");
            window.location.href = "{{url('login')}}";
          } else{
            $("#password").val('');
            $("#password_confirmation").val('');
            $("#register_submit").html('Register now');
            $("#error_div").removeClass('hide').html(msg.dialog);
          }
        }
      });
    }));
  });

  $(document).ready(function() {
    $('select').material_select();

    $('select').change(function(){
        var data = $('select option:selected').val();
        if(data == 2){
            $('#nurse_text').removeClass('hide');
        } else {
            $('#nurse_text').addClass('hide');
        }
    });
  });
</script>
@stop

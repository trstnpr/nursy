@extends('layouts.app')

<!-- Main Content -->
@section('content')
<section id="forgot_pass">
    <div class="wrap">
        <div class="container">
            <div class="row login-page">

                <div class="col m4 s12 offset-m4">
                    <div class="center">
                        <img src="{{ asset('assets/images/logo/nursy-logo-2.png') }}" alt="" class="responsive-img">
                        <p class="center login-form-text">Password Reset</p>
                    </div>

                    <form id="forgotpass" method="post" role="form" action="{{ url('/password/email') }}">
                        {!! csrf_field() !!}
                        <div class="row card-panel blue-grey darken-4">
                            <div class="input-field col s12">
                                <input class="validate" type="email" id="email" name="email" required="required" placeholder="Your email"/>
                                <label for="email" data-error="wrong" data-success="right">Email</label>
                            </div>

                            <div class="input-field col s12 hide" id="error_div">
                                <div class="card-panel pink ligthen-2">
                                    <strong>Error message</strong>
                                </div>
                            </div>

                            <div class="inout-field col s12">
                                <button type="submit" class="btn btn-large waves-effect waves-light blue-grey col s12">
                                    <i class="material-icons right">send</i>Send Password Reset Link
                                </button>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <p class="center medium-small sign-up">Remember now? <a href="{{ url('/login') }}" class="light-blue-text">Login</a></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

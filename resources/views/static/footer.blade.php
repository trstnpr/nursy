<footer id="footer">

	<div class="content-1">
		
		<div class="container">

			<div class="row">
				
				<div class="col s12 m3 l3">
					<h5 class="h5 white-text">NAVIGATION</h5>
					<br>
					<ul class="navigation">
						<li><a class="white-text" href="{{ url('') }}">Home</a></li>
						<li><a class="white-text" onClick="scrollPage('content-2');" style="cursor: pointer;">Services</a></li>
						<li><a class="white-text" onClick="scrollPage('content-3');" style="cursor: pointer;">Features</a></li>
						<li><a class="white-text" onClick="scrollPage('content-5');" style="cursor: pointer;">How It Works</a></li>
						<li><a class="white-text " href="{{ url('/contact')}}">Contact Us</a></li>
						<li><a class="white-text" href="#!">About Us</a></li>
					</ul>
				</div>

				<div class="col s12 m6 l6" align="center">

					<div class="row">
						<div class="col s12 m12 l12">
							<h4 class="white-text bold">nursy.co</h4>
							<h5 class="h5 white-text">Healthcare at Your Doorstep</h5>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m12 l12">
							<h5 class="white-text">Subscribe to our newsletter</h5>

							<div class="input-field">
						          <input placeholder="your email address" type="text" class="grey darken-2 white-text radius center-text" />
							</div>	
						</div>
					</div>

				</div>

				<div class="col s12 m3 l3" align="right">
					<h5 class="h5 white-text"><i class="mdi-maps-local-phone tiny"></i> CONTACT US</h5>
					<br>
					<address class="white-text">
						28th Floor Pacific Star Bldg.,
						Makati Ave. corner Buendia Ave., Makati City,
						Philippines
						<br>
						<br>
						 +(632) 822 - 2755
					 	<br>
					 	<br>
					 	info@nursy.co
					 	<br>
					 	<br>
					 	<a href="#" class="white-text" style="margin: 10px;"><i class="fa fa-facebook fa-lg"></i></a>
					 	<a href="#" class="white-text" style="margin: 10px;"><i class="fa fa-twitter fa-lg"></i></a>

					</address>
				</div>

			</div>

		</div>

	</div>

	<div class="content-2">
		
		<div class="container">
			
			<div class="row">
				
				<div class="col s12 m12 l12" align="center">
					
					<p class="white-text">&copy; Copyright@nursy.co</p>

				</div>

			</div>

		</div>

	</div>

</footer>

<!-- MATERIALIZE CSS -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/materialize/materialize.css')}}" media="screen,projection">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/materialize/customize.css')}}" media="screen,projection">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

<!-- Font awesome -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.min.css')}}">

<!-- NURSY CSS -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/nursy.css')}}" media="screen,projection">

<!-- ALERTIFY CSS -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/alertify/alertify.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/alertify/default.min.css')}}">

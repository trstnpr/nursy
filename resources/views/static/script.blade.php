<!--  JQUERY -->
<script type="text/javascript" src="{{ asset('assets/js/jquery/jquery-1.11.2.min.js')}}"></script>

<!-- materialize -->
<script type="text/javascript" src="{{ asset('assets/js/materialize/materialize.min.js')}}"></script>

<!-- alertify -->
<script type="text/javascript" src="{{ asset('assets/js/alertify/alertify.min.js')}}"></script>

<!-- zabuto -->
<script type="text/javascript" src="{{ asset('assets/js/zabuto/zabuto_calendar.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/zabuto/zabuto_calendar.css')}}" />

<script type="text/javascript">
	/*Preloader*/
  $(window).load(function() {
    setTimeout(function() {
      $('body').addClass('loaded');      
    }, 200);
  }); 

  //Main Left Sidebar Chat
  $('.chat-collapse').sideNav({
    menuWidth: 240,
    edge: 'right',
  });
  $('.chat-close-collapse').click(function() {
    $('.chat-collapse').sideNav('hide');
  });
  $('.chat-collapsible').collapsible({
    accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
  });

  function makeSticky(){
  	var myWindow=$(window);
  	var myHeader=$(".navbar");
  	var myMenu=$("#menu");
  	myWindow.scroll(function(){
  		if(myWindow.scrollTop()==0){
  			myHeader.addClass("transparent");
  			myHeader.removeClass("light-blue");
  			myMenu.removeClass("light-blue");
  			myMenu.addClass("transparent");
  		}else{
  			myHeader.removeClass("transparent");
  			myHeader.addClass("light-blue");
  			myMenu.addClass("light-blue");
  			myMenu.removeClass("transparent");
  		}
  	});
  }

  	$(document).ready(function(){
  		makeSticky();

      $('.scroll').click(function(){
        var id = $(this).closest(".scroll").attr("id");
        $('html, body').animate({scrollTop: $('.'+id).offset().top},1000);
      });

  	});


    var xmlhttp = new XMLHttpRequest();

    function scrollPage(id) {
        window.location =  "{{ url('') }}?id="+id;

    }


    function get(){
      var QueryString = function () {
        // This function is anonymous, is executed immediately and 
        // the return value is assigned to QueryString!
        var query_string = {};
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
          var pair = vars[i].split("=");
              // If first entry with this name
          if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
              // If second entry with this name
          } else if (typeof query_string[pair[0]] === "string") {
            var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
            query_string[pair[0]] = arr;
              // If third or later entry with this name
          } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
          }
        } 
          return query_string;
      }();

       if($('.'+QueryString.id).length){
          $('html, body').animate({scrollTop: $('.'+QueryString.id).offset().top},1000);
       }
        
    }

    $(window).load(function(){
        get();
    });



</script>
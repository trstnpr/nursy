<footer class="page-footer transparent">
    <div class="footer-copyright transparent">
      	<div class="container grey-text text-darken-4" align="center">
	        <span>Copyright © 2016 <a class="light-blue-text text-darken-2" href="{{ url('') }}" target="_blank">Nursy.co</a> All rights reserved.</span>
	        <span class="right"> Design and Developed by <a class="orange-text text-darken-4" href="http://devhub.ph/">{ Devhub }</a></span>
        </div>
    </div>
</footer>
<!-- CUSTOM JS -->
<script type="text/javascript" src="{{ asset('assets/js/nursy.js')}}"></script>

<script>

	function uploadImage(){
	  document.getElementById("profile_image").click();
	}

	$(document).ready(function() {

		/*SELECT BOX ISSUE*/
	    $('select').material_select();

		$('#profile_image').change(function(){
			var myfile = $(this).val();
		   	var ext = myfile.split('.').pop();
		   	if(ext=="jpeg" || ext=="jpg" || ext=="png" || ext=="gif"){
		    	$('#profile_image_form').submit();
		   	} else{
		    	alert("Required file types: jpeg, jpg, png, gif");
		   	}
		});

		$("#profile_image_form").on('submit',(function(e) {
          e.preventDefault();
          $.ajax({
            url: "{{url('/uploadProfileImage')}}", // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,        // To send DOMDocument or non processed data file it is set to false
            success: function(data)   // A function to be called if request succeeds
            {
              var msg = JSON.parse(data);
              if(msg.result == 'success'){
              	location.reload();
              } else{
              	alert(msg.result);
              }
            }
          });
        }));
	});
</script>
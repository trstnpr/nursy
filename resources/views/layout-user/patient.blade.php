<!DOCTYPE html>
<html lang="en">
<head>
    <!-- GLOBAL HEADER -->
    @include('static.header')

    <!-- GLOBAL STYLE -->
    @include('static.style')

    <!-- CUSTOM STYLE -->
    @yield('style')

</head>
<body>
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>        
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>

    <section id="profile">
        <!-- TOP NAVIGATION -->
        @include('layout-user.navigation.patient-nav')

        <div id="main">
            <div class="wrapper">
                 <!-- SIDE NAVIGATION -->
                @include('layout-user.navigation.patient-side')
               
               <section id="content">
                <!-- GLOBAL CONTENT-->
                @yield('content')

                <!-- GLOBAL FOOTER -->
                @include('layout-user.static.footer')
                </section>

            </div>
        </div>
    </section>

    <!-- GLOBAL JavaScripts -->
    @include('static.script')

    <!-- USER JavaScripts -->
    @include('layout-user.static.script')

    <!-- CUSTOM SCRIPT -->
    @yield('customScript')
</body>
</html>

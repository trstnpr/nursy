<!-- START LEFT SIDEBAR NAV-->
<aside id="left-sidebar-nav">
    <ul id="slide-out" class="side-nav fixed leftside-navigation" style="overflow-y: auto;">

        <li class="user-details light-blue darken-3">
            <div class="row">
                <div class="col l10" align="center">
                    @if(is_null($user->image))
                    <div class="profile-image z-depth-1" style="background-image: url('{{ asset('assets/images/avatars/img_parent.png')}}');">
                        <div class="edit_image">
                            <button class="btn-floating btn-large cyan" onClick="uploadImage();" style="top:2.5em;">
                                <i class="mdi-image-camera-alt"></i>

                            </button>
                            <form method="POST" id="profile_image_form" enctype="multipart/form-data">
                                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                <input type="file" id="profile_image" name="profile_image" style="display:none;" />
                            </form>
                        </div>
                    </div>
                    @else
                    <div class="profile-image z-depth-1" style="background-image: url('https://s3-ap-southeast-1.amazonaws.com/nursy/{{ $user->image }}');">
                        <div class="edit_image">
                            <button class="btn-floating btn-large cyan" onClick="uploadImage();" style="top:2.5em;">
                                <i class="mdi-image-camera-alt"></i>

                            </button>
                            <form method="POST" id="profile_image_form" enctype="multipart/form-data">
                                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                <input type="file" id="profile_image" name="profile_image" style="display:none;" />
                            </form>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col s12 m12 l12">
                    <p class="white-text profile-btn">{{$user->first_name}} {{$user->last_name}}</p>
                    <p class="user-roal">{{ ($user->role_id == 2 && $user->nurse_type != null) ? $user->nurse_type : 'Nurse' }}</p>
                </div>
            </div>
        </li>

        <li class="bold">
            <a href="{{ url('nurse/userDashBoard')}}" class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Dashboard</a>
        </li>
		
		<li class="bold">
            <a href="{{ url('nurse/services') }}" class="waves-effect waves-cyan"><i class="mdi-maps-local-pharmacy"></i> Services</a>
        </li>

        <li class="bold">
            <a href="{{ url('nurse/appointment') }}" class="waves-effect waves-cyan"><i class="mdi-action-history"></i> Appointments</a>
        </li>

        <li class="bold">
            <a href="{{url('nurse/schedule')}}" class="waves-effect waves-cyan"><i class="mdi-notification-event-available"></i> Schedule</a>
        </li>
		<li class="li-hover"><div class="divider"></div></li>
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
                <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-action-settings"></i> Settings</a>
                    <div class="collapsible-body">
                        <ul>
                            <li>
                                <a href="{{ url('nurse/profileSettings') }}">Profile Settings</a>
                            </li>  

                            <li>
                                <a href="{{ url('nurse/accountSettings') }}">Account Settings</a>
                            </li> 

                            <li>
                                <a href="{{ url('nurse/passwordSettings') }}">Password Management</a>
                            </li>                                        
                        </ul>
                    </div>
                </li>

            </ul>
        </li>
		<li class="bold">
            <a href="{{url('logout')}}" class="waves-effect waves-cyan"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
        </li>
		<!-- <li class="bold">
            <a href="{{url('dhtmlx')}}" class="waves-effect waves-cyan"><i class="mdi-notification-event-available"></i> DHTMLX Test</a>
        </li> -->
        <li class="li-hover">
            
        </li>
        
    </ul>
    <a href="#" data-activates="slide-out" style="top: -55px; right: 24px;" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only btn-large transparent z-depth-0">
        <i class="mdi-navigation-menu" ></i>
    </a>
</aside>
<!-- END LEFT SIDEBAR NAV-->
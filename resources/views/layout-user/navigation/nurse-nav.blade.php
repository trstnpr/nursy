<header id="header" class="page-topbar">
	<!-- start header nav-->
	<div class="navbar-fixed">
		<nav class="light-blue">
			<div class="nav-wrapper">
				<h1 class="logo-wrapper">
					<a href="{{url('nurse/userDashBoard')}}" class="brand-logo darken-1">
						<img src="{{ asset('assets/images/logo/nursy-logo-2.png') }}" width="120px" alt="Nursy Logo">
					</a> 
					<span class="logo-text">Nursy.co</span>
				</h1>
				<ul class="right hide-on-med-and-down">
					<li>    
						<a href="{{url('/logout')}}" class="waves-effect waves-block waves-light tooltipped" data-position="left" data-delay="50" data-tooltip="LOGOUT"><i class="mdi-hardware-keyboard-tab"></i></a>                              
					</li>
				</ul>
			</div>
		</nav>
	</div>
	<!-- end header nav-->
</header>
<!-- CUSTOM JS -->
<script type="text/javascript" src="{{ asset('assets/js/nursy.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/js/alertify/alertify.min.js')}}"></script>

<script>
	$(document).ready(function() {
		$('select').material_select();
	});
</script>
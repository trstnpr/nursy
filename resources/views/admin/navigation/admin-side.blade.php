<!-- START LEFT SIDEBAR NAV-->
<aside id="left-sidebar-nav">
    <ul id="slide-out" class="side-nav fixed leftside-navigation blue-grey darken-4" style="overflow-y: auto;">
        <li class="user-details blue-grey darken-3">
            <div class="row">
                <div class="col s12 m12 l12">
                    <p class="white-text profile-btn">Nursy Admin</p>
                    <p class="user-roal">Administrator</p>
                </div>
            </div>
        </li>

        <li class="bold">
            <a href="{{ url('/admin')}}" class="waves-effect waves-cyan white-text"><i class="mdi-action-dashboard"></i> Dashboard</a>
        </li>

        <li class="bold">
            <a href="{{url('admin/nurseList')}}" class="waves-effect waves-cyan white-text"><i class="mdi-action-assignment-ind"></i> Nurse</a>
        </li>

        <li class="bold">
            <a href="{{url('admin/patientList')}}" class="waves-effect waves-cyan white-text"><i class="mdi-action-account-circle"></i> Patient</a>
        </li>
        <li class="divider"></li>
		<li class="bold">
            <a href="{{url('admin/password')}}" class="waves-effect waves-cyan white-text"><i class="mdi-action-lock"></i> Password Management</a>
        </li>
		<li class="bold">
            <a href="{{url('admin/logout')}}" class="waves-effect waves-cyan white-text"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
        </li>
    </ul>
    <a href="#" data-activates="slide-out" style="top: -75px; right: 24px;" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only btn-large transparent z-depth-0">
        <i class="mdi-navigation-menu" ></i>
    </a>
</aside>
<!-- END LEFT SIDEBAR NAV-->
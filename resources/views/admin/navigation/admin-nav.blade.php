<header id="header" class="page-topbar">
	<!-- start header nav-->
	<div class="navbar-fixed">
		<nav class="blue-grey">
			<div class="nav-wrapper">
				<h1 class="logo-wrapper">
					<a href="{{url('/admin')}}" class="brand-logo">
						<img src="{{ asset('assets/images/logo/nursy-logo-2.png') }}" width="120px" alt="Nursy Logo">
					</a> 
					<span class="logo-text">Nursy.co</span>
				</h1>
				<ul class="right hide-on-med-and-down">
					<li>    
						<a href="{{url('/admin/logout')}}" class="waves-effect waves-block waves-light tooltipped" data-position="left" data-delay="50" data-tooltip="LOGOUT"><i class="mdi-hardware-keyboard-tab left"></i> Logout</a>                              
					</li>
				</ul>
			</div>
		</nav>
	</div>
	<!-- end header nav-->
</header>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- GLOBAL HEADER -->
    @include('static.header')

    <!-- GLOBAL STYLE -->
    @include('static.style')

    <!-- CUSTOM STYLE -->
    @yield('style')

</head>
<body class="cyan">
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>        
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->
   
    <!-- GLOBAL CONTENT-->
    @yield('content')

    <!-- GLOBAL JavaScripts -->
    @include('static.script')

    <!-- CUSTOM SCRIPT -->
    @yield('customScript')
</body>
</html>

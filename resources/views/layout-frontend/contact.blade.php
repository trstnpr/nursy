<!DOCTYPE html>
<html lang="en">
<head>
    <!-- GLOBAL HEADER -->
    @include('static.header')

    <!-- GLOBAL STYLE -->
    @include('static.style')

    <!-- CUSTOM STYLE -->
    @yield('style')

</head>
<body>
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>        
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->

    @include('layout-frontend.contact-nav')
   
    <!-- GLOBAL CONTENT-->
    @yield('content')


    <!-- GLOBAL FOOTER -->
    @include('static.footer')


    <!-- GLOBAL JavaScripts -->
    @include('static.script')

    <!-- CUSTOM SCRIPT -->
    @yield('customScript')
</body>
</html>

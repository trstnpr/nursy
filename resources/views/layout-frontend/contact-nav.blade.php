<div class="navbar-fixed">
	<nav class="light-blue z-depth-0">
	  	<div class="container">
	  		<div class="nav-wrapper">
			     <h1 class="logo-wrapper">
                    <a href="{{ url('') }}" class="brand-logo darken-1">
                        <img src="{{ asset('assets/images/logo/nursy-logo-2.png') }}" width="120px" alt="Nursy Logo">
                    </a> 
                    <span class="logo-text">Nursy.co</span>
                </h1>

			    <ul class="right">
			      @if(is_null($user))
			      <li><a href="{{ url('/login') }}"><span style="font-size: 1.2em;">LOGIN</span></a></li>
				  @else
				  <li><a href="{{ ($user->role_id == 2) ? url('nurse/userDashBoard') : url('patient/userDashBoard') }}"><span style="font-size: 1.2em;">{{$user->first_name}} {{$user->last_name}}</span></a></li>
				  @endif
			    </ul>
			</div>
	  	</div>	
	</nav>
</div>
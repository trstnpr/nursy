<section class="navigation-bar">

<!-- TOP MENU FIXED -->
	<div class="navbar-fixed">
		<nav class="transparent z-depth-0 navbar">
		  	<div class="container">
		  		<div class="nav-wrapper">
		  			<a href="#" data-activates="chat-out" class="button-collapse chat-collapse transparent z-depth-0 waves-effect" id="menu"><i class="mdi-navigation-menu"></i></a>
		  			<ul class="left hide-on-med-and-down">
				      @if(is_null($user))
				      <li><a href="{{ url('/login') }}"><span style="font-size:1.2em;">LOGIN</span></a></li>
				      @else

				      <li>
				      	  <!-- Dropdown Trigger -->
						  <a class="dropdown-button valign-wrapper" href='#' data-activates='dropdown1'>
						  	@if(is_null($user->image))
						  	<img src="{{ asset('assets/images/avatars/img_parent.png')}}" alt="{{strtoupper($user->first_name.' '.$user->last_name)}}" class="responsive-img circle left" width="50px">
						  	@else
						  	<img src="https://s3-ap-southeast-1.amazonaws.com/nursy/{{ $user->image }}" alt="{{strtoupper($user->first_name.' '.$user->last_name)}}" class="responsive-img circle left" width="50px">
						  	@endif
						  	<span style="font-size: 1.2em;text-indent: 5px;">
						  	{{ strtoupper($user->first_name.' '.$user->last_name) }}
						  	</span>
						  </a>

						  <!-- Dropdown Structure -->
						  <ul id='dropdown1' class='dropdown-content'>
							@if($user->role_id == 2)
							<li><a href="{{url('nurse/userDashBoard')}}">Dashboard</a></li>
							<li><a href="{{url('nurse/accountSettings')}}">Account Settings</a></li>
							<li><a href="{{url('nurse/profileSettings')}}">Profile Settings</a></li>
							@else
							<li><a href="{{url('patient/userDashBoard')}}">Dashboard</a></li>
						    <li><a href="{{url('patient/accountSettings')}}">Account Settings</a></li>
							@endif
							<li class="divider"></li>
						    <li><a href="{{url('/logout')}}">Logout</a></li>
						  </ul>
				      </li>
				      @endif
				    </ul>
				    <ul id="nav-mobile" class="right hide-on-med-and-down" style="font-weight: 600;">
						<li><a href="#!" class="scroll" id="content-1">Home</a></li>
				        <li><a href="#!" class="scroll" id="content-2">Services</a></li>
				        <li><a href="#!" class="scroll" id="content-3">Features</a></li>
				        <li><a href="#!" class="scroll" id="content-5">How It Works</a></li>
				        <li><a href="#!" class="white-text">About us</a></li>
				        <li><a href="{{ url('/contact') }}" class="white-text">Contact Us</a></li>
					</ul>
		  		</div>
		  	</div>	
		</nav>
	</div>



    <ul id="chat-out" class="side-nav">
    	@if(is_null($user))
    	<li class="active light-blue center"><a href="{{url('/login')}}" style="color:#fff;letter-spacing:0.7em;"><big>LOGIN</big></a></li>
    	@else
    	<li class="active">
    		<!-- Dropdown Trigger -->
			<a class="dropdown-button valign-wrapper" href='#' data-activates='dropdown2'>
				<span style="font-size: 1.2em;">
					{{ strtoupper($user->first_name.' '.$user->last_name) }}
				</span>
				@if(is_null($user->image))
				<img src="{{ asset('assets/images/avatars/img_parent.png')}}" alt="{{strtoupper($user->first_name.' '.$user->last_name)}}" class="responsive-img circle right" width="40px">
				@else
				<img src="https://s3-ap-southeast-1.amazonaws.com/nursy/{{ $user->image }}" alt="{{strtoupper($user->first_name.' '.$user->last_name)}}" class="responsive-img circle right" width="40px">
				@endif
			</a>
			<ul id='dropdown2' class='dropdown-content'>
				<li><a href="{{url('userDashBoard')}}">Dashboard</a></li>
				<li><a href="{{url('/accountSettings')}}">Account Settings</a></li>
				@if($user->role_id == 2)
				<li><a href="{{url('/profileSettings')}}">Profile Settings</a></li>
				@endif
				<li class="divider"></li>
				<li><a href="{{url('/logout')}}">Logout</a></li>
			</ul>
    	</li>
    	@endif
        <li><a class="scroll" id="content-1">Home</a></li>
        <li><a class="scroll" id="content-2">Services</a></li>
        <li><a class="scroll" id="content-3">Features</a></li>
        <li><a class="scroll" id="content-5">How It Works</a></li>
        <li><div class="divider"></div></li>
        <li class="red"><a href="#!" class="white-text"><i class="fa fa-info-circle fa-lg"></i>&nbsp;&nbsp;About us</a></li>
        <li class="light-blue"><a href="{{ url('/contact') }}" class="white-text"><i class="fa fa-phone fa-lg"></i>&nbsp;&nbsp;Contact Us</a></li>
    </ul>
</section>